## [0.20.1](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.20.0...v0.20.1) (2025-02-14)

### Miscellaneous Chores

* **deps:** update dependencies ([28a8f39](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/28a8f3960550b499216c6ac81b6e26abacb16fc7))

# [0.20.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.19.0...v0.20.0) (2025-02-10)


### Features

* add the read_repository_file_lines tool ([16516cc](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/16516cccb8656883d5eb0f0d4fcc7aaff953f38c))

# [0.19.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.18.0...v0.19.0) (2025-01-26)


### Bug Fixes

* extra leading " at the beginning of the task message ([21ad034](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/21ad034fd14ffd7b3ac0d25d550479b9678f66c5))
* invalid target name for handoffs to MergeRequestDiscussionAgent ([8041603](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/8041603b6a8101de8dd1ae0fce9a7977ff7dcb7b))
* unknown column fragments.commit_hash ([c642c23](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/c642c234e7a051d5daebe21c0206775c95ca1e62))


### Features

* implement the MergeRequestSummaryAgent ([a024ff2](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/a024ff2ffd5c0879081730dcc16e549e6c0f5637))
* sanitize agent system prompts ([0196ad1](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/0196ad1afcdc8e1146a9619eae7840fec5648989))
* use "N/A" has the default project description ([329936e](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/329936e76de196c702b19e2bb5c918be2933b500))
* use a file queue to ensure the index is exhaustive ([5281d9d](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/5281d9d5850c6a83a18f5bab70bc3c855fc29011))

# [0.18.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.17.0...v0.18.0) (2025-01-19)


### Bug Fixes

* make the discussion agents reply in the same language as the user ([b5220a5](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/b5220a51d1db0912bcf3b922e7bb9148dac49d24))


### Features

* check a document with the same path/commit hash already exist before inserting it ([959b2ce](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/959b2ce90abd81389e64ae36a47be8ea50666feb))
* **gitlab-ci:** cache the repository index SQLite databases ([78298b6](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/78298b659f49efdbec6536b23321fe6d83b18dd0))
* print a warning for todos that get filtered out ([8c9a47c](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/8c9a47c5459f9e4d1347bab31283f4a98986ba5a))

# [0.17.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.16.0...v0.17.0) (2025-01-12)


### Features

* add support for raw text files indexing/search ([edff5f3](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/edff5f32a463a704d39832647af995484f227c90))

# [0.16.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.15.0...v0.16.0) (2025-01-09)


### Bug Fixes

* add missing GitlabHttpError handling in find_repository_files ([aedf745](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/aedf7453279cf8eee0cbe00d68f95b13a99beaef))


### Features

* add the find_repository_files tool ([2d85174](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/2d85174484405c070784baacd18616278731e6b1))
* add the list_project_languages tool ([1fe22eb](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/1fe22eba9e466e679c7641abff58c6482c6b42dc))
* raise retryable GitLab HTTP errors to inform the LLM and let it eventually retry ([bfb7c2f](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/bfb7c2f7a55b1a86672ae20068c6498dadda7ef5))

# [0.15.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.14.1...v0.15.0) (2025-01-09)


### Features

* update the SummaryAgent prompt to focus on the last message of the user ([d53a42c](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/d53a42cba8baeba035a6f83b78c41c95548c5f0d))

## [0.14.1](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.14.0...v0.14.1) (2025-01-09)


### Bug Fixes

* remove the unimplemented Markdown indexer ([3b370cd](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/3b370cd3801f67936e0e38045f1b9392bdb06b31))

# [0.14.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.13.0...v0.14.0) (2025-01-09)


### Bug Fixes

* make sure Note.type is always set ([1f7791e](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/1f7791e85e953050f5fa81e6eb42d14ae02b1fd2))
* model field closed_by must be optional ([24ad958](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/24ad958e82f2fa110b20989219c6043482f16616))


### Features

* add the search_for_functions() tool ([be34d8c](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/be34d8cc74edbd8a18f243a560e17fc4c2369172))

# [0.13.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.12.0...v0.13.0) (2025-01-08)


### Bug Fixes

* discard todos created by the bot itself to avoid infinite loops ([28c1781](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/28c178169fa25ea5b447da44dc0b273e62175dfb))
* discussion note filtering edge case ([7a77974](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/7a779747cdcdc6e72716265c577724e279960599))
* invalid 'tools[1].function.description': string too long ([a37c411](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/a37c41194e0539514185c6d22aba49ed7731ffc4))
* make handle_gitlab_error re-raise unretryable errors ([31b07e6](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/31b07e622a0298cd90f539483702b3e68776e0bd))
* missing 1 required positional argument: 'user' ([d1719ff](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/d1719ff784599e5e7fd78d094ca3a0d35cbee85d))


### Features

* implement custom models for Issues and a summary agent ([6ce5c2d](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/6ce5c2df88ddfd9e9ec7f5dcd2307798a90484d2))
* tweak the MR/issue system prompts ([3f462ad](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/3f462adfb0615702acb91b8628356e8c85754692))

# [0.12.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.11.1...v0.12.0) (2025-01-07)


### Bug Fixes

* **gitlab-ci:** configure git to rebase when pulling to avoid divergent branches ([855d775](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/855d7754bb48eeb1edf7d310c836801968b05615))
* handle GitLab system notes ([13b0f97](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/13b0f979a0c850f1282cb66f7c3f7810a5cf0f54))
* remove deprecated multi-threading CLI option ([83e44fc](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/83e44fca03bd23e1c25078047dac45e60bff2ac9))


### Features

* add an optional `note_type` arg to the`list_issue_notes` tool ([4748ee7](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/4748ee744d31a264450580bf2a8dc2caf7fafc06))
* add an optional `note_type` arg to the`list_merge_request_notes` tool ([d926cd2](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/d926cd25da6fcbfc27826a507d3ade803d7dc39e))
* better documentation for the comment listing tools ([33386bb](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/33386bbc81de0ce7634495879b0114dd5acb17a7))

## [0.11.1](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.11.0...v0.11.1) (2025-01-07)


### Bug Fixes

* **gitlab-ci:** error "Failed to spawn: `wally.py`" ([09ccc61](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/09ccc6171b33934b9a168c3c2e1f01fd4a64916e))
* **gitlab-ci:** error "git: command not found" ([0d84c95](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/0d84c95afbb7b222c370a6e1177132126646a133))

# [0.11.0](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.10.1...v0.11.0) (2025-01-07)


### Bug Fixes

* unit test imports ([5ec68f4](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/5ec68f44cbabd71c09cdefc5c56372fd511226b1))


### Features

* add issue and MR management agents ([071da02](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/071da02d6f0fb7c2fab08615ec58c57f4d8711dc))
* **gitlab-ci:** set the default model to gpt-4o ([d7962fa](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/d7962fa44e005a1981c40d47c0bf519428089681))
* implement a RepositoryManagementAgent capable of reading repository files ([4542800](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/4542800194b78ae98c2e6545fa16c1b4e320e0c3))
* implement function_to_tool() ([efecec5](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/efecec5f9225a774e503f8e3fa8fa96cf4b2a7f1))
* multi-agent system first draft ([0388c29](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/0388c294c1dc959da2202bdbf1145a51cc6e929d))
* re-add support for merge request diff notes ([3ecd53e](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/3ecd53ea487ce03fdc925aa23788ef1c163d85c3))

## [0.10.1](https://gitlab.com/lx-industries/wally-the-wobot/wally/compare/v0.10.0...v0.10.1) (2025-01-05)


### Bug Fixes

* typos ([52782ce](https://gitlab.com/lx-industries/wally-the-wobot/wally/commit/52782ce9a31950e5d3af26bb4881c3662690d8b0))

# [0.10.0](https://gitlab.com/wally-the-wobot/wally/compare/v0.9.0...v0.10.0) (2024-05-15)


### Features

* set gpt-4o as the default model ([bd62cbf](https://gitlab.com/wally-the-wobot/wally/commit/bd62cbf04a89392087e23b51095069c733754ecc))

# [0.9.0](https://gitlab.com/wally-the-wobot/wally/compare/v0.8.1...v0.9.0) (2023-09-24)


### Features

* update the prompt to leverage Mermaid diagrams ([#60](https://gitlab.com/wally-the-wobot/wally/issues/60)) ([4a72c3a](https://gitlab.com/wally-the-wobot/wally/commit/4a72c3a8862600e2e90a148a6904df31faa406be))

## [0.8.1](https://gitlab.com/wally-the-wobot/wally/compare/v0.8.0...v0.8.1) (2023-09-17)


### Bug Fixes

* urls not properly expanded ([#49](https://gitlab.com/wally-the-wobot/wally/issues/49)) ([08d4deb](https://gitlab.com/wally-the-wobot/wally/commit/08d4debf1cb0f99f79ba56c687135b358f286abd))

# [0.8.0](https://gitlab.com/wally-the-wobot/wally/compare/v0.7.0...v0.8.0) (2023-08-27)


### Bug Fixes

* flags not at the start of the expression ([3c1c7f3](https://gitlab.com/wally-the-wobot/wally/commit/3c1c7f3689451e6287e1aeeb918965754ef30b63))
* inconsistent URL handling ([#49](https://gitlab.com/wally-the-wobot/wally/issues/49)) ([9810b3b](https://gitlab.com/wally-the-wobot/wally/commit/9810b3bd3d061d67d76addd3c2189f028a04cd6e))
* unused --openai-model option ([d094899](https://gitlab.com/wally-the-wobot/wally/commit/d0948990ed019afb72519d9d82da902f2c6ebba1))


### Features

* **openai:** use the "assistant" role when relevant ([#53](https://gitlab.com/wally-the-wobot/wally/issues/53)) ([21271b9](https://gitlab.com/wally-the-wobot/wally/commit/21271b904311c35dec7a8003c5315a8da2338dc7))

# [0.7.0](https://gitlab.com/wally-the-wobot/wally/compare/v0.6.0...v0.7.0) (2023-07-07)


### Features

* set the default OpenAI model to gpt-4 ([a0de46b](https://gitlab.com/wally-the-wobot/wally/commit/a0de46b70fa0b95f546cdbc5deb047b348bf2c38))

# [0.6.0](https://gitlab.com/wally-the-wobot/wally/compare/v0.5.1...v0.6.0) (2023-06-25)


### Features

* configurable OpenAI model ([#37](https://gitlab.com/wally-the-wobot/wally/issues/37)) ([c8459fc](https://gitlab.com/wally-the-wobot/wally/commit/c8459fca0e636ad7fd2ceec3edd77e02c0127b78))

## [0.5.1](https://gitlab.com/wally-the-wobot/wally/compare/v0.5.0...v0.5.1) (2023-05-07)


### Bug Fixes

* **ci:** invalid revision range in the check:commitlint job ([a158cb6](https://gitlab.com/wally-the-wobot/wally/commit/a158cb6d383a0c2fe14e88c696a2d257ef00c10d))

# [0.5.0](https://gitlab.com/wally-the-wobot/wally/compare/v0.4.0...v0.5.0) (2023-05-03)


### Bug Fixes

* **ci:** commit/push CHANGELOG.md upon release ([229c232](https://gitlab.com/wally-the-wobot/wally/commit/229c232878971375ca6cdc0edcf201381a80bac4))


### Features

* **ci:** add a job to check the semantic-release config ([55b89fe](https://gitlab.com/wally-the-wobot/wally/commit/55b89fe08d9f5ac1737533d869b4f9f3806a6699))
