# Wally <!-- omit in toc -->

![](./img/wally-the-wobot.png)

[![pipeline](https://gitlab.com/wally-the-wobot/wally/badges/main/pipeline.svg)](https://gitlab.com/wally-the-wobot/wally/-/pipelines?page=1&scope=branches&ref=main)
[![bot](https://gitlab.com/wally-the-wobot/the-wobot/badges/main/pipeline.svg?key_text=bot)](https://gitlab.com/wally-the-wobot/the-wobot/-/pipelines?page=1&scope=branches&ref=main)

Wally is a GitLab assistant powered by ChatGPT, an AI language model developed by OpenAI. With Wally, you can interact with your GitLab project using natural language and receive helpful suggestions and feedback from the AI.

For example, Wally can:

- 🤔 Answer questions in issues and suggest how to implement features or fix bugs.
- 🔧 Refactor code/find bugs in merge requests.
- 📚 Explain changes in commits.
- 📖 Suggest documentation.

Wally leverages GitLab's to-do system as a message queue and GitLab CI to run its REPL. So there is nothing to install and you can start using it in mere seconds (cf [How To Use](#2-how-to-use))!

Wally is FOSS. So if you like it, please make a [donation](#6-donations)!

The name "Wally" is a reference to the comic book character Wally West, also known as the Flash, as well as the title character from the 2008 Pixar film WALL-E.

- [1. Features](#1-features)
- [2. How To Use](#2-how-to-use)
  - [2.1. On GitLab.com](#21-on-gitlabcom)
  - [2.2. On your own GitLab instance](#22-on-your-own-gitlab-instance)
  - [2.3. Locally](#23-locally)
- [3. Privacy](#3-privacy)
- [4. Configuration](#4-configuration)
- [5. License](#5-license)
- [6. Contributing](#6-contributing)

## 1. Features

- 📃 Support for issue, merge request and commit comments.
- 🧠 Context aware. The agent knows:
  - Authors usernames and job titles.
  - The time/date of each message.
  - The content of the lines of the file being commented on (for merge request and commits).
- 🔗 File links expansion: links to repository files are expanded into their actual content, with support for line ranges.
- 🙊 Privacy savvy: the agent does not know what you don't tell it. Only the messages and the content their refer to (ex: diff lines) are sent to the OpenAI API (see [Privacy Notice](#3-privacy) for more information).

## 2. How To Use

### 2.1. On GitLab.com

Just invite @WallyTheWobot to your project (`Project information > Members > Invite members`), and start mentioning @WallyTheWobot in your comments.

The logs are available directly in GitLab CI [here](https://gitlab.com/wally-the-wobot/the-wobot/-/pipelines?page=1&scope=branches&ref=main).

### 2.2. On your own GitLab instance

1. Create a new GitLab project.
2. Add a `.gitlab-ci.yml` file with the following content (replace `main` with [the actual version](https://gitlab.com/wally-the-wobot/wally/-/releases) you want to use):
    ```yml
    include:
      - remote: https://gitlab.com/wally-the-wobot/wally/-/raw/main/.gitlab-ci/WallyTheWobot.gitlab-ci.yml

    wally-the-wobot:
      extends: .wally-the-wobot
    ```
3. Create a scheduled pipeline in `CI/CD > Schedules`.
4. Configure the via the CI variables in `Settings > CI/CD` (cf [Configuration](#4-configuration)).

### 2.3. Locally

1. Clone the repository: `git clone https://gitlab.com/wally-the-wobot/wally.git`
2. Enter the directory: `cd wally`
3. Install the dependencies: `pip install -r ./requirements.txt`
4. Create a `.env` file add some configuration (cf [Configuration](#4-configuration)).
5. Run: `python wally.py`.

## 3. Privacy

**If you talk to @WallyTheRobot on GitLab.com**, keep in mind:

- It runs inside GitLab CI Runner [here](https://gitlab.com/wally-the-wobot/the-wobot/-/pipelines?page=1&scope=branches&ref=main).
- The runner is setup on a free tiers AWS EC2 instance.
- The content of the discussion is sent to the OpenAI chat completion API.

So you might want to check the corresponding privacy policy for those services:

- https://about.gitlab.com/privacy/
- https://aws.amazon.com/privacy/
- https://openai.com/policies/privacy-policy

**Even if you're running with it on your own instance/GitLab CI runner/server** (cf [How to use on your own instance](#22-on-your-own-gitlab-instance)) check at least [the OpenAI privacy policy](https://openai.com/policies/privacy-policy).

## 4. Configuration

If you are running `wally.py` locally, it also accept command line arguments. Run `./wally.py --help` for more information.

If you are rolling with your own instance or if you want to run it locally, the following environment/CI variables are available:

- `WALLY_GITLAB_TOKEN`: your GitLab personal access token.
- `WALLY_GITLAB_URL`: the URL of your GitLab repository. For example, `https://gitlab.com/`.
- `WALLY_LOG_LEVEL`: The logging level for Wally. Can be set to `debug`, `info`, `warning`, `error`, or `critical` (default: `info`).
- `WALLY_DRY_RUN`: Run Wally in dry-run mode. In dry-run mode, Wally will not perform any write operation  (default: `false`).
- `WALLY_MAX_RUN_TIME`: Maximum run time in seconds for Wally's main loop (default: `120`).
- `WALLY_SLEEP_TIME`: Sleep time in seconds between iterations in Wally's main loop (default: `30`).
- `WALLY_NAMESPACE_FILTER`: Filter todos by project namespace (regex) (default: `.*`).
- `WALLY_OPENAI_API_KEY`: your OpenAI API key.
- `WALLY_OPENAI_MODEL`: the OpenAI model to use (default: `gpt-4o`).
- `WALLY_MAX_TOKENS`: the maximum number of tokens allowed in the responses generated by OpenAI chat completion API (default: `512`).

Environment variables will be read from a `.env` file if it exists.

## 5. License

GNU AGPL v3.

See [LICENSE](https://gitlab.com/wally-the-wobot/wally/-/blob/main/LICENSE) for the
full license.

## 6. Contributing

- [Bugs](https://gitlab.com/wally-the-wobot/wally/-/issues/?sort=priority&state=opened&label_name%5B%5D=bug&first_page_size=20)
- [Features](https://gitlab.com/wally-the-wobot/wally/-/issues/?sort=priority&state=opened&label_name%5B%5D=enhancement&first_page_size=20)

If you would like to contribute to Wally, please fork the repository and submit a merge request with your changes. You can also report issues and suggest new features using the GitLab issue tracker.

We welcome contributions from all developers, regardless of skill level or experience. Together, we can build a better GitLab assistant powered by AI.
