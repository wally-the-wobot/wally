import re
from typing import List, cast
import unittest
from unittest.mock import MagicMock, Mock

import gitlab
from gitlab import Gitlab, GitlabHttpError
import gitlab.v4
from gitlab.v4.objects import (
    CurrentUser,
    Issue,
    MergeRequest,
    Project,
    ProjectIssueDiscussion,
    ProjectIssueNote,
    ProjectMergeRequestDiscussion,
    ProjectMergeRequestNote,
    Todo,
)
import gitlab.v4.objects
from wally.models import Commit, Diff, Note
from wally.platform import todo_must_be_discarded
from wally.tools import (
    get_merge_request_by_id,
    list_commit_diffs,
    list_merge_request_commits,
    list_repository_files,
    get_issue_by_id,
    get_note_type,
    get_todo_discussion_id,
    list_issue_notes,
    list_issues,
    list_merge_request_notes,
    list_merge_requests,
)


class TestTodoMustBeDiscarded(unittest.TestCase):
    def setUp(self):
        mock_manager = Mock()

        # Mock user representing the bot
        self.bot_user = CurrentUser(mock_manager, attrs={"id": 1})

        # Mock todos
        self.todo_by_bot = Todo(mock_manager, attrs={"id": 1, "author": {"id": 1}, "action_name": "directly_addressed"})
        self.todo_by_user = Todo(
            mock_manager, attrs={"id": 2, "author": {"id": 2}, "action_name": "directly_addressed"}
        )
        self.todo_mentioned = Todo(mock_manager, attrs={"id": 3, "author": {"id": 3}, "action_name": "mentioned"})
        self.todo_other_action = Todo(mock_manager, attrs={"id": 4, "author": {"id": 4}, "action_name": "other_action"})

    def test_discard_todo_by_bot(self):
        # Should discard todos created by the bot
        self.assertTrue(todo_must_be_discarded(self.todo_by_bot, self.bot_user))

    def test_keep_todo_by_user(self):
        # Should not discard todos created by other users
        self.assertFalse(todo_must_be_discarded(self.todo_by_user, self.bot_user))

    def test_keep_todo_mentioned(self):
        # Should not discard todos that are mentioned
        self.assertFalse(todo_must_be_discarded(self.todo_mentioned, self.bot_user))

    def test_discard_todo_other_action(self):
        # Should discard todos with actions other than directly_addressed or mentioned
        self.assertTrue(todo_must_be_discarded(self.todo_other_action, self.bot_user))


class TestGetNoteType(unittest.TestCase):
    def test_system_note_type(self):
        note = {"system": True}
        self.assertEqual(get_note_type(note), "SystemNote")

    def test_discussion_note_type(self):
        note = {"system": False, "type": "DiscussionNote"}
        self.assertEqual(get_note_type(note), "DiscussionNote")

    def test_default_discussion_note_type(self):
        note = {"system": False, "type": None}
        self.assertEqual(get_note_type(note), "DiscussionNote")


class TestListIssueNotes(unittest.TestCase):
    def setUp(self):
        # Mock GitLab client and objects
        self.mock_gitlab_client = Mock()
        self.mock_project = Mock(spec=Project)
        self.mock_issue_manager = Mock()
        self.mock_issue = Mock(spec=Issue)
        self.mock_discussions_manager = Mock()
        self.mock_discussion = Mock(spec=ProjectIssueDiscussion)
        self.mock_notes_manager = Mock()

        self.mock_note1 = Mock(
            spec=ProjectIssueNote,
            **{
                "asdict.return_value": {
                    "id": 1,
                    "system": False,
                    "type": "DiscussionNote",
                    "created_at": "2025-01-01T12:00:00Z",
                    "updated_at": "2025-01-01T12:10:00Z",
                    "body": "Comment 1",
                    "project_id": 101,
                    "author": {"id": 1, "username": "user1"},
                }
            },  # type: ignore
        )
        self.mock_note2 = Mock(
            spec=ProjectIssueNote,
            **{
                "asdict.return_value": {
                    "id": 2,
                    "system": True,
                    "created_at": "2025-01-02T12:00:00Z",
                    "updated_at": "2025-01-02T12:10:00Z",
                    "body": "Comment 2",
                    "project_id": 101,
                    "author": {"id": 2, "username": "user2"},
                }
            },  # type: ignore
        )
        self.mock_note3 = Mock(
            spec=ProjectIssueNote,
            **{
                "asdict.return_value": {
                    "id": 3,
                    "system": False,
                    "type": None,
                    "created_at": "2025-01-03T12:00:00Z",
                    "updated_at": "2025-01-03T12:10:00Z",
                    "body": "Comment 3",
                    "project_id": 101,
                    "author": {"id": 3, "username": "user3"},
                }
            },  # type: ignore
        )

        # Set up the mock GitLab client
        self.mock_gitlab_client.projects.get.return_value = self.mock_project

        # Set up the mock project
        self.mock_project.issues = self.mock_issue_manager
        self.mock_issue_manager.get.return_value = self.mock_issue

        # Set up the mock issue
        self.mock_issue.notes = self.mock_notes_manager  # Add 'notes' attribute
        self.mock_issue.discussions = self.mock_discussions_manager  # Add 'discussions' attribute
        self.mock_notes_manager.list.return_value = [self.mock_note1, self.mock_note2, self.mock_note3]
        self.mock_discussions_manager.get.return_value = self.mock_discussion

        # Set up the mock discussion
        self.mock_discussion.attributes = {
            "notes": [
                {
                    "id": 4,
                    "type": "DiscussionNote",  # Previously `type: None`
                    "created_at": "2025-01-04T12:00:00Z",
                    "updated_at": "2025-01-04T12:10:00Z",
                    "body": "Comment 4",
                    "project_id": 101,
                    "author": {"id": 4, "username": "user4"},
                }
            ]
        }

    def test_list_all_notes(self):
        self.mock_issue.notes.list.return_value = [self.mock_note1, self.mock_note2]

        result = list_issue_notes(self.mock_gitlab_client, project_id=1, issue_iid=1, note_type=None)

        self.assertIsNotNone(result)

        notes: List[Note] = cast(List[Note], result)

        self.assertEqual(len(notes), 2)
        self.assertEqual(notes[0].id, 1)
        self.assertEqual(notes[0].body, "Comment 1")
        self.assertEqual(notes[0].author.id, 1)
        self.assertEqual(notes[0].author.username, "user1")
        self.assertEqual(notes[0].created_at, "2025-01-01T12:00:00Z")
        self.assertEqual(notes[1].id, 2)
        self.assertEqual(notes[1].body, "Comment 2")
        self.assertEqual(notes[1].author.id, 2)
        self.assertEqual(notes[1].author.username, "user2")

    def test_list_discussion_notes(self):
        self.mock_issue.notes.list.return_value = [self.mock_note1, self.mock_note2, self.mock_note3]

        result = list_issue_notes(self.mock_gitlab_client, project_id=1, issue_iid=1, note_type="DiscussionNote")

        self.assertIsNotNone(result)

        notes: List[Note] = cast(List[Note], result)

        self.assertEqual(len(notes), 2)
        self.assertEqual(notes[0].id, 1)
        self.assertEqual(notes[0].type, "DiscussionNote")
        self.assertEqual(notes[0].author.id, 1)
        self.assertEqual(notes[0].author.username, "user1")
        self.assertEqual(notes[1].id, 3)
        self.assertEqual(notes[1].type, "DiscussionNote")
        self.assertEqual(notes[1].author.id, 3)
        self.assertEqual(notes[1].author.username, "user3")

    def test_list_system_notes(self):
        self.mock_issue.notes.list.return_value = [self.mock_note1, self.mock_note2, self.mock_note3]

        result = list_issue_notes(self.mock_gitlab_client, project_id=1, issue_iid=1, note_type="SystemNote")

        self.assertIsNotNone(result)

        notes: List[Note] = cast(List[Note], result)

        self.assertEqual(len(notes), 1)
        self.assertEqual(notes[0].id, 2)
        self.assertEqual(notes[0].type, "SystemNote")
        self.assertEqual(notes[0].author.id, 2)
        self.assertEqual(notes[0].author.username, "user2")

    def test_list_notes_from_discussion(self):
        self.mock_issue.discussions.get.return_value = self.mock_discussion

        result = list_issue_notes(self.mock_gitlab_client, project_id=1, issue_iid=1, discussion_id="123")

        self.assertIsNotNone(result)

        notes: List[Note] = cast(List[Note], result)

        self.assertEqual(len(notes), 1)
        self.assertEqual(notes[0].id, 4)
        self.assertEqual(notes[0].body, "Comment 4")
        self.assertEqual(notes[0].author.id, 4)
        self.assertEqual(notes[0].author.username, "user4")

    def test_handle_gitlab_http_error(self):
        self.mock_gitlab_client.projects.get.side_effect = GitlabHttpError("GitLab error")

        with self.assertRaises(GitlabHttpError):
            list_issue_notes(self.mock_gitlab_client, project_id=1, issue_iid=1)


class TestListMergeRequestNotes(unittest.TestCase):
    def setUp(self):
        # Mock GitLab client and objects
        self.mock_gitlab_client = Mock()
        self.mock_project = Mock(spec=Project)
        self.mock_merge_request_manager = Mock()
        self.mock_merge_request = Mock(spec=MergeRequest)
        self.mock_discussions_manager = Mock()
        self.mock_discussion = Mock(spec=ProjectMergeRequestDiscussion)
        self.mock_notes_manager = Mock()

        self.mock_note1 = Mock(
            spec=ProjectMergeRequestNote,
            **{
                "asdict.return_value": {
                    "id": 1,
                    "system": False,
                    "type": "DiscussionNote",
                    "created_at": "2025-01-01T12:00:00Z",
                    "updated_at": "2025-01-01T12:10:00Z",
                    "body": "Comment 1",
                    "project_id": 101,
                    "author": {"id": 1, "username": "user1"},
                }
            },  # type: ignore
        )
        self.mock_note2 = Mock(
            spec=ProjectMergeRequestNote,
            **{
                "asdict.return_value": {
                    "id": 2,
                    "system": True,
                    "created_at": "2025-01-02T12:00:00Z",
                    "updated_at": "2025-01-02T12:10:00Z",
                    "body": "Comment 2",
                    "project_id": 101,
                    "author": {"id": 2, "username": "user2"},
                }
            },  # type: ignore
        )
        self.mock_note3 = Mock(
            spec=ProjectMergeRequestNote,
            **{
                "asdict.return_value": {
                    "id": 3,
                    "system": False,
                    "type": None,
                    "created_at": "2025-01-03T12:00:00Z",
                    "updated_at": "2025-01-03T12:10:00Z",
                    "body": "Comment 3",
                    "project_id": 101,
                    "author": {"id": 3, "username": "user3"},
                }
            },  # type: ignore
        )

        # Set up the mock GitLab client
        self.mock_gitlab_client.projects.get.return_value = self.mock_project

        # Set up the mock project
        self.mock_project.mergerequests = self.mock_merge_request_manager
        self.mock_merge_request_manager.get.return_value = self.mock_merge_request

        # Set up the mock merge request
        self.mock_merge_request.notes = self.mock_notes_manager  # Add 'notes' attribute
        self.mock_merge_request.discussions = self.mock_discussions_manager  # Add 'discussions' attribute
        self.mock_notes_manager.list.return_value = [self.mock_note1, self.mock_note2, self.mock_note3]
        self.mock_discussions_manager.get.return_value = self.mock_discussion

        # Set up the mock discussion
        self.mock_discussion.attributes = {
            "notes": [
                {
                    "id": 4,
                    "type": "DiscussionNote",  # Previously `type: None`
                    "created_at": "2025-01-04T12:00:00Z",
                    "updated_at": "2025-01-04T12:10:00Z",
                    "body": "Comment 4",
                    "project_id": 101,
                    "author": {"id": 4, "username": "user4"},
                }
            ]
        }

    def test_list_all_notes(self):
        self.mock_merge_request.notes.list.return_value = [self.mock_note1, self.mock_note2]

        result = list_merge_request_notes(self.mock_gitlab_client, project_id=1, merge_request_iid=1, note_type=None)

        self.assertEqual(len(result), 2)
        self.assertEqual(result[0].id, 1)
        self.assertEqual(result[0].body, "Comment 1")
        self.assertEqual(result[0].author.id, 1)
        self.assertEqual(result[0].author.username, "user1")
        self.assertEqual(result[0].created_at, "2025-01-01T12:00:00Z")
        self.assertEqual(result[1].id, 2)
        self.assertEqual(result[1].body, "Comment 2")
        self.assertEqual(result[1].author.id, 2)
        self.assertEqual(result[1].author.username, "user2")

    def test_list_discussion_notes(self):
        self.mock_merge_request.notes.list.return_value = [self.mock_note1, self.mock_note2, self.mock_note3]

        result = list_merge_request_notes(
            self.mock_gitlab_client, project_id=1, merge_request_iid=1, note_type="DiscussionNote"
        )

        self.assertEqual(len(result), 2)
        self.assertEqual(result[0].id, 1)
        self.assertEqual(result[0].type, "DiscussionNote")
        self.assertEqual(result[0].author.id, 1)
        self.assertEqual(result[0].author.username, "user1")
        self.assertEqual(result[1].id, 3)
        self.assertEqual(result[1].type, "DiscussionNote")
        self.assertEqual(result[1].author.id, 3)
        self.assertEqual(result[1].author.username, "user3")

    def test_list_system_notes(self):
        self.mock_merge_request.notes.list.return_value = [self.mock_note1, self.mock_note2, self.mock_note3]

        result = list_merge_request_notes(
            self.mock_gitlab_client, project_id=1, merge_request_iid=1, note_type="SystemNote"
        )

        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].id, 2)
        self.assertEqual(result[0].type, "SystemNote")
        self.assertEqual(result[0].author.id, 2)
        self.assertEqual(result[0].author.username, "user2")

    def test_list_notes_from_discussion(self):
        self.mock_merge_request.discussions.get.return_value = self.mock_discussion

        result = list_merge_request_notes(
            self.mock_gitlab_client, project_id=1, merge_request_iid=1, discussion_id="123"
        )

        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].id, 4)
        self.assertEqual(result[0].body, "Comment 4")
        self.assertEqual(result[0].author.id, 4)
        self.assertEqual(result[0].author.username, "user4")

    def test_handle_gitlab_http_error(self):
        self.mock_gitlab_client.projects.get.side_effect = GitlabHttpError("GitLab error")

        with self.assertRaises(GitlabHttpError):
            list_merge_request_notes(self.mock_gitlab_client, project_id=1, merge_request_iid=1)


class TestGetTodoDiscussionId(unittest.TestCase):
    def setUp(self):
        self.mock_gitlab_client = MagicMock()
        self.project_id = "45037508"
        self.todo_id = "492538410"
        self.target_url = "https://gitlab.com/lx-industries/wally-the-wobot/wally/-/issues/76#note_2289519991"
        self.target_type = "Issue"
        self.target_id = "76"

    def test_get_discussion_id_success(self):
        # Mock the project and issue retrieval
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_issue = mock_project.issues.get.return_value

        # Mock the discussions list
        mock_discussion = MagicMock()
        mock_discussion.attributes = {"notes": [{"id": 2289519991}]}
        mock_issue.discussions.list.return_value = [mock_discussion]

        # Call the function
        discussion_id = get_todo_discussion_id(
            self.todo_id, self.project_id, self.target_url, self.target_type, self.target_id, self.mock_gitlab_client
        )

        # Assert the discussion ID is returned
        self.assertEqual(discussion_id, mock_discussion.id)

    def test_get_discussion_id_not_found(self):
        # Mock the project and issue retrieval
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_issue = mock_project.issues.get.return_value

        # Mock the discussions list to be empty
        mock_issue.discussions.list.return_value = []

        # Call the function
        discussion_id = get_todo_discussion_id(
            self.todo_id, self.project_id, self.target_url, self.target_type, self.target_id, self.mock_gitlab_client
        )

        # Assert None is returned when discussion ID is not found
        self.assertIsNone(discussion_id)


class TestGetIssueById(unittest.TestCase):
    def setUp(self):
        self.mock_gitlab_client = MagicMock()
        self.project_id = "45037508"
        self.issue_iid = "76"

    def test_get_issue_by_id_success(self):
        # Set up mock data for a successful case
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_issue = mock_project.issues.get.return_value
        mock_issue.asdict.return_value = {
            "iid": 76,
            "title": "Sample Issue",
            "description": "This is a sample issue.",
            "state": "opened",
            "created_at": "2025-01-01T00:00:00.000Z",
            "updated_at": "2025-01-01T00:00:00.000Z",
            "closed_at": None,
            "closed_by": None,
            "author": {"id": 1, "username": "author"},
            "assignee": None,
            "assignees": [{"id": 2, "username": "assignee"}],
            "project_id": 45037508,
        }

        maybe_issue = get_issue_by_id(self.project_id, self.issue_iid, self.mock_gitlab_client)

        self.assertIsNotNone(maybe_issue)

        issue: Issue = cast(Issue, maybe_issue)

        self.assertEqual(issue.iid, 76)
        self.assertEqual(issue.title, "Sample Issue")

    def test_get_issue_by_id_not_found(self):
        # Simulate GitlabHttpError for not found
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_project.issues.get.side_effect = GitlabHttpError("Issue not found")

        with self.assertRaises(GitlabHttpError):
            get_issue_by_id(self.project_id, self.issue_iid, self.mock_gitlab_client)


class TestListIssues(unittest.TestCase):
    def setUp(self):
        # Correctly initialize the mock_gitlab_client to simulate GitLab client behavior
        self.mock_gitlab_client = MagicMock()
        self.project_id = "45037508"

    def test_list_issues_success(self):
        # Set up mock data for a successful case
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_project.issues.list.return_value = [
            MagicMock(
                asdict=lambda: {
                    "iid": 76,
                    "title": "Sample Issue",
                    "description": "This is a sample issue.",
                    "state": "opened",
                    "created_at": "2025-01-01T00:00:00.000Z",
                    "updated_at": "2025-01-01T00:00:00.000Z",
                    "closed_at": None,
                    "closed_by": None,
                    "author": {"id": 1, "username": "author"},
                    "assignee": None,
                    "assignees": [{"id": 2, "username": "assignee"}],
                    "project_id": 45037508,
                }
            )
        ]

        result = list_issues(self.mock_gitlab_client, self.project_id)

        self.assertIsNotNone(result)

        issues: List[Issue] = cast(List[Issue], result)

        self.assertEqual(len(issues), 1)
        self.assertEqual(issues[0].iid, 76)

    def test_list_issues_empty(self):
        # Simulate no issues found
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_project.issues.list.return_value = []

        issues = list_issues(self.mock_gitlab_client, self.project_id)

        self.assertEqual(issues, [])


class TestListMergeRequests(unittest.TestCase):
    def setUp(self):
        # Mock GitLab client and objects
        self.mock_gitlab_client = Mock()

        # Create a MagicMock for the merge request object
        self.mock_merge_request = MagicMock(spec=MergeRequest)

        self.mock_merge_request.asdict.return_value = {
            "iid": 1,
            "title": "Test MR",
            "description": "This is a test merge request",
            "state": "opened",
            "created_at": "2025-01-01T00:00:00Z",
            "updated_at": "2025-01-02T00:00:00Z",
            "closed_at": None,
            "closed_by": None,
            "author": {"id": 1, "username": "author"},
            "assignee": None,
            "assignees": [],
            "project_id": 1,
            "has_conflicts": False,
            "source_branch": "42-feature",
            "target_branch": "main",
        }

        # Set up the mock GitLab client
        self.mock_gitlab_client.projects.get.return_value = Mock(
            mergerequests=Mock(list=Mock(return_value=[self.mock_merge_request]))
        )

    def test_list_merge_requests(self):
        # Call the function with the mock client
        result = list_merge_requests(self.mock_gitlab_client, project_id=1)

        expected = self.mock_merge_request.asdict()

        mrs = cast(List[MergeRequest], result)

        # Assertions to verify the behavior
        self.assertEqual(len(mrs), 1)
        self.assertEqual(mrs[0].iid, expected["iid"])
        self.assertEqual(mrs[0].title, expected["title"])


class TestFindFilesFunction(unittest.TestCase):
    def setUp(self):
        # Mock GitLab client and projects
        self.mock_gitlab_client = Mock(spec=Gitlab)
        self.mock_project = Mock()
        self.mock_gitlab_client.projects = Mock(spec=Project)
        self.mock_gitlab_client.projects.get = Mock()
        self.mock_gitlab_client.projects.get.return_value = self.mock_project

        # Mock project repository tree
        self.mock_project.repository_tree.return_value = [
            {"path": "src/main.py"},
            {"path": "src/utils/helpers.py"},
            {"path": "README.md"},
        ]

    def test_list_repository_files_with_valid_regex(self):
        # Test with a valid regex pattern
        pattern = r".*\.py$"
        expected_files = ["src/main.py", "src/utils/helpers.py"]
        result = list_repository_files(self.mock_gitlab_client, 1, pattern)
        self.assertEqual(result, expected_files)

    def test_list_repository_files_with_invalid_regex(self):
        # Test with an invalid regex pattern
        pattern = r"*invalid["
        with self.assertRaises(re.error):
            list_repository_files(self.mock_gitlab_client, 1, pattern)

    def test_list_repository_files_no_matches(self):
        # Test with a pattern that matches no files
        pattern = r".*\.txt$"
        expected_files = []
        result = list_repository_files(self.mock_gitlab_client, 1, pattern)
        self.assertEqual(result, expected_files)


class TestGetMergeRequestById(unittest.TestCase):
    def setUp(self):
        self.mock_gitlab_client = MagicMock()
        self.project_id = "45037508"
        self.merge_request_id = "123"

    def test_get_merge_request_by_id_success(self):
        # Set up mock data for a successful case
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_merge_request = mock_project.mergerequests.get.return_value
        mock_merge_request.asdict.return_value = {
            "iid": 123,
            "title": "Sample Merge Request",
            "description": "This is a sample merge request.",
            "state": "opened",
            "created_at": "2025-01-01T00:00:00.000Z",
            "updated_at": "2025-01-01T00:00:00.000Z",
            "closed_at": None,
            "closed_by": None,
            "author": {"id": 1, "username": "author"},
            "assignee": None,
            "assignees": [{"id": 2, "username": "assignee"}],
            "project_id": 45037508,
            "has_conflicts": False,  # Add this field
            "source_branch": "feature-branch",  # Add this field
            "target_branch": "main",  # Add this field
        }

        result: MergeRequest = cast(
            MergeRequest, get_merge_request_by_id(self.project_id, self.merge_request_id, self.mock_gitlab_client)
        )

        self.assertIsNotNone(result)
        self.assertEqual(result.iid, 123)
        self.assertEqual(result.title, "Sample Merge Request")

    def test_get_merge_request_by_id_not_found(self):
        # Simulate GitlabHttpError for not found
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_project.mergerequests.get.side_effect = GitlabHttpError("Merge Request not found")

        with self.assertRaises(GitlabHttpError):
            get_merge_request_by_id(self.project_id, self.merge_request_id, self.mock_gitlab_client)


class TestListMergeRequestCommits(unittest.TestCase):
    def setUp(self):
        self.mock_gitlab_client = MagicMock()
        self.project_id = "45037508"
        self.merge_request_id = "123"

    def test_list_merge_request_commits_success(self):
        # Set up mock data for a successful case
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_commit1 = MagicMock(spec=gitlab.v4.objects.ProjectCommit)
        mock_commit1.asdict.return_value = {
            "id": "ed899a2f4b50b4370feeea94676502b42383c746",
            "short_id": "ed899a2f4b5",
            "title": "Replace sanitize with escape once",
            "author_name": "Example User",
            "author_email": "user@example.com",
            "authored_date": "2021-09-20T11:50:22.001+00:00",
            "committer_name": "Administrator",
            "committer_email": "admin@example.com",
            "committed_date": "2021-09-20T11:50:22.001+00:00",
            "created_at": "2021-09-20T11:50:22.001+00:00",
            "message": "Replace sanitize with escape once",
            "parent_ids": ["6104942438c14ec7bd21c6cd5bd995272b3faff6"],
            "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/ed899a2f4b50b4370feeea94676502b42383c746",
            "trailers": {},
            "extended_trailers": {},
        }
        mock_commit2 = MagicMock(spec=gitlab.v4.objects.ProjectCommit)
        mock_commit2.asdict.return_value = {
            "id": "6104942438c14ec7bd21c6cd5bd995272b3faff6",
            "short_id": "6104942438c",
            "title": "Sanitize for network graph",
            "author_name": "randx",
            "author_email": "user@example.com",
            "committer_name": "ExampleName",
            "committer_email": "user@example.com",
            "created_at": "2021-09-20T09:06:12.201+00:00",
            "message": "Sanitize for network graph\nCc: John Doe <johndoe@gitlab.com>",
            "parent_ids": ["ae1d9fb46aa2b07ee9836d49862ec4e2c46fbbba"],
            "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/ed899a2f4b50b4370feeea94676502b42383c746",
            "trailers": {"Cc": "Jane Doe <janedoe@gitlab.com>"},
            "extended_trailers": {"Cc": ["John Doe <johndoe@gitlab.com>", "Jane Doe <janedoe@gitlab.com>"]},
        }
        mock_merge_request = mock_project.mergerequests.get.return_value
        mock_merge_request.commits.return_value = [mock_commit1, mock_commit2]

        result = list_merge_request_commits(
            project_id=self.project_id, merge_request_iid=self.merge_request_id, gitlab_client=self.mock_gitlab_client
        )

        self.assertIsNotNone(result)

        commits: List[Commit] = cast(List[Commit], result)

        self.assertEqual(len(commits), 2)
        self.assertEqual(commits[0].id, "ed899a2f4b50b4370feeea94676502b42383c746")
        self.assertEqual(commits[0].title, "Replace sanitize with escape once")
        self.assertEqual(commits[1].id, "6104942438c14ec7bd21c6cd5bd995272b3faff6")
        self.assertEqual(commits[1].title, "Sanitize for network graph")

    def test_list_merge_request_commits_empty(self):
        # Simulate no commits found
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_merge_request = mock_project.mergerequests.get.return_value
        mock_merge_request.commits.return_value = []

        result = list_merge_request_commits(
            project_id=self.project_id, merge_request_iid=self.merge_request_id, gitlab_client=self.mock_gitlab_client
        )

        self.assertEqual(result, [])


class TestListCommitDiffs(unittest.TestCase):
    def setUp(self):
        self.mock_gitlab_client = MagicMock()
        self.project_id = "45037508"
        self.commit_id = "abc123"

    def test_list_commit_diffs_success(self):
        # Set up mock data for a successful case
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_commit = mock_project.commits.get.return_value
        mock_commit.diff.return_value = [
            {
                "diff": "@@ -71,6 +71,8 @@\n sudo -u git -H bundle exec rake migrate_keys RAILS_ENV=production",
                "new_path": "doc/update/5.4-to-6.0.md",
                "old_path": "doc/update/5.4-to-6.0.md",
                "a_mode": None,
                "b_mode": "100644",
                "new_file": False,
                "renamed_file": False,
                "deleted_file": False,
            },
            {
                "diff": "@@ -71,6 +71,8 @@\n sudo -u git -H bundle exec rake migrate_keys RAILS_ENV=production",
                "new_path": "main.py",
                "old_path": "main.py",
                "a_mode": "100644",
                "b_mode": "100644",
                "new_file": False,
                "renamed_file": False,
                "deleted_file": False,
            },
        ]

        result = list_commit_diffs(
            project_id=self.project_id, commit_sha=self.commit_id, gitlab_client=self.mock_gitlab_client
        )

        self.assertIsNotNone(result)

        diffs: List[Diff] = cast(List[Diff], result)

        self.assertEqual(len(diffs), 2)  # Ensure the mock returns one diff
        self.assertEqual(diffs[0].old_path, "doc/update/5.4-to-6.0.md")
        self.assertEqual(diffs[0].new_path, "doc/update/5.4-to-6.0.md")

    def test_list_commit_diffs_empty(self):
        # Simulate no diffs found
        mock_project = self.mock_gitlab_client.projects.get.return_value
        mock_commit = mock_project.commits.get.return_value
        mock_commit.diffs.return_value = []

        result = list_commit_diffs(
            project_id=self.project_id, commit_sha=self.commit_id, gitlab_client=self.mock_gitlab_client
        )

        self.assertEqual(result, [])


if __name__ == "__main__":
    unittest.main()
