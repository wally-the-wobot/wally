import logging
from typing import Any, Dict, List, Set, Tuple, cast
import os
import gitlab
import sqlite3
from gitlab.v4.objects import Project
import sqlite_vec
import openai
from tree_sitter import Language, Parser
import tree_sitter_python
from sqlite_vec import serialize_float32

from wally.models import FragmentType, FileStatus, IndexSearchResult

PARSERS = {
    ".py": tree_sitter_python.language,
    ".md": None,
    ".txt": None,
}
FILES_TABLE = "files"
FILES_TABLE_SCHEMA = f"""(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    path TEXT NOT NULL,
    commit_hash TEXT NOT NULL,
    status TEXT NOT NULL CHECK (status IN ('{FileStatus.pending.value}', '{FileStatus.processing.value}', '{FileStatus.done.value}'))
)"""  # noqa: E501
EMBEDDING_SIZE = 1536
EMBEDDING_MODEL = "text-embedding-ada-002"
EMBEDDINGS_TABLE = "embeddings"
FRAGMENTS_TABLE = "fragments"
FRAGMENTS_TABLE_SCHEMA = f"""(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    file_id INTEGER NOT NULL,
    type TEXT NOT NULL CHECK (type IN ('{FragmentType.function.value}', '{FragmentType.text.value}')),
    content TEXT NOT NULL,
    embedding_id INTEGER,
    FOREIGN KEY (embedding_id) REFERENCES {EMBEDDINGS_TABLE}(rowid),
    FOREIGN KEY (file_id) REFERENCES {FILES_TABLE}(id)
)"""

logger = logging.getLogger(__name__)


class RepositoryIndex:
    def __init__(self, gitlab_client: gitlab.Gitlab, project_id: str | int):
        logger.info(f"Initializing RepositoryIndex for project_id: {project_id}")

        self._gitlab_client: gitlab.Gitlab = gitlab_client
        self._project_id: str | int = project_id
        self._connection: sqlite3.Connection = self._connect()

        self._initialize()

    @property
    def database_file_path(self) -> str:
        return f"repository_index_{self._project_id}.sqlite"

    def _connect(self) -> sqlite3.Connection:
        db = sqlite3.connect(self.database_file_path)
        db.enable_load_extension(True)
        sqlite_vec.load(db)
        db.enable_load_extension(False)
        (
            sqlite_version,
            vec_version,
        ) = db.execute("SELECT sqlite_version(), vec_version()").fetchone()
        logger.debug(f"Connected to SQLite, sqlite_version={sqlite_version}, vec_version={vec_version}")
        return db

    def _initialize(self):
        logger.info(f"Initializing repository index for project id: {self._project_id}")

        cursor: sqlite3.Cursor = self._connection.cursor()

        cursor.execute(f"""
            CREATE VIRTUAL TABLE IF NOT EXISTS {EMBEDDINGS_TABLE}
            USING vec0(vector FLOAT[{EMBEDDING_SIZE}])
        """)
        cursor.execute(f"""
            CREATE TABLE IF NOT EXISTS {FILES_TABLE} {FILES_TABLE_SCHEMA};
        """)
        cursor.execute(f"""
            CREATE TABLE IF NOT EXISTS {FRAGMENTS_TABLE} {FRAGMENTS_TABLE_SCHEMA};
        """)

        self._connection.commit()
        # Initialize embeddings for all files
        self._update_file_queue()
        self._process_file_queue()

    def _list_changed_files(self, project: Project) -> Tuple[Set[str], str]:
        # Get the current commit hash of the default branchcurrent_commit_hash
        current_commit_hash = list(project.commits.list(ref_name=project.default_branch, all=False))[0].id

        logger.debug(f"Current commit hash: {current_commit_hash}")

        cursor: sqlite3.Cursor = self._connection.cursor()

        # Retrieve the last stored commit hash from the database.
        # The file queue is inserted/updated via a transaction, thus:
        # - the last commit hash should be the last one inserted
        # - all the files with the last commit hash should be in the queue
        cursor.execute(
            f"""
            SELECT commit_hash
            FROM {FILES_TABLE}
            ORDER BY id DESC
            LIMIT 1
            """
        )
        last_commit_hash = cursor.fetchone()

        if last_commit_hash:
            last_commit_hash = last_commit_hash[0]
            # Get the list of files changed since the last commit
            result = cast(Dict[str, Any], project.repository_compare(last_commit_hash, current_commit_hash))
            changes = result.get("diffs", [])
            changed_files = {change["new_path"] for change in changes}
        else:
            # If no previous commit hash, consider all files as changed
            changed_files = {file["path"] for file in project.repository_tree(recursive=True, all=True, per_page=100)}

        # Filter changed files to include only those with extensions in PARSERS
        filtered_files = {
            file_path for file_path in changed_files if any(file_path.endswith(ext) for ext in PARSERS.keys())
        }

        return (filtered_files, current_commit_hash)

    def _update_file_queue(self) -> Tuple[int, int, int]:
        project: Project = self._gitlab_client.projects.get(self._project_id)
        paths, commit_hash = self._list_changed_files(project)
        num_inserted = 0
        num_reset = 0
        num_skipped = 0
        cursor: sqlite3.Cursor = self._connection.cursor()

        file_queue = cursor.execute(
            f"""
            SELECT id, path, status
            FROM {FILES_TABLE}
            WHERE commit_hash = ?
            """,
            (commit_hash,),
        ).fetchall()

        try:
            cursor.execute("BEGIN;")
            for path in paths:
                file_id, status = next(
                    ((id, status) for (id, file_path, status) in file_queue if file_path == path), (None, None)
                )
                if status == FileStatus.done:
                    logger.debug(f"File `{path}` already with commit hash `{commit_hash}` already processed: skipping.")
                    num_skipped += 1
                elif status == FileStatus.processing:
                    logger.debug(f"File `{path}` already with commit hash `{commit_hash}` being processed: resetting.")
                    cursor.execute(
                        f"UPDATE {FILES_TABLE} SET status = ? WHERE path = ? AND commit_hash = ?",
                        (FileStatus.pending.value, path, commit_hash),
                    )
                    cursor.execute(f"DELETE FROM {FRAGMENTS_TABLE} WHERE file_id = ?", (file_id,))
                    num_reset += 1
                else:
                    logger.debug(f"Inserting file `{path}` with commit hash `{commit_hash}` into the queue.")
                    cursor.execute(
                        f"INSERT INTO {FILES_TABLE} (path, commit_hash, status) VALUES (?, ?, ?)",
                        (path, commit_hash, FileStatus.pending.value),
                    )
                    num_inserted += 1

            # Commit the transaction
            cursor.execute("COMMIT;")
        except Exception as e:
            # Rollback the transaction in case of error
            cursor.execute("ROLLBACK;")
            raise e

        return (num_inserted, num_reset, num_skipped)

    def _process_file_queue(self) -> Tuple[int, int]:
        num_pending = 0
        num_processed = 0
        project: Project = self._gitlab_client.projects.get(self._project_id)
        cursor: sqlite3.Cursor = self._connection.cursor()
        files = cursor.execute(
            f"""
            SELECT *
            FROM {FILES_TABLE}
            WHERE status = ?
            """,
            (FileStatus.pending.value,),
        ).fetchall()
        num_pending = len(files)
        logger.info(f"Processing {num_pending} files")
        for file in files:
            file_id, file_path, commit_hash, status = file
            cursor.execute(f"UPDATE {FILES_TABLE} SET status = ? WHERE id = ?", (FileStatus.processing.value, file_id))
            self._connection.commit()
            file_content = project.files.get(file_path=file_path, ref=commit_hash).decode()
            self._parse_file(file_id, file_path, commit_hash, file_content)
            cursor.execute(f"UPDATE {FILES_TABLE} SET status = ? WHERE id = ?", (FileStatus.done.value, file_id))
            self._connection.commit()
            num_processed += 1
            logger.info(f"Processed file: {file_path}, {num_processed}/{num_pending}")
        return (num_pending, num_processed)

    def _parse_file(self, file_id: int, file_path: str, commit_hash: str, file_content: bytes):
        # Parse the file with Tree-sitter
        parser = PARSERS[os.path.splitext(file_path)[1]]
        if parser:
            logger.debug(f"Parsing file : {file_path}")
            parser = Parser(Language(parser()))
            tree = parser.parse(file_content)

            # Traverse the tree to find logical scopes
            def traverse_tree(file_path, node, source_code):
                if node.type == "function_definition":
                    scope = source_code[node.start_byte : node.end_byte]
                    self._insert_fragment(
                        file_id=file_id,
                        commit_hash=commit_hash,
                        type=FragmentType.function,
                        content=scope,
                    )
                for child in node.children:
                    traverse_tree(file_path, child, source_code)

            traverse_tree(file_path, tree.root_node, file_content)
        # Otherwise, treat the file as plain text.
        else:
            logger.warning(f"No parser found for file: {file_path}")
            content = file_content.decode(encoding="utf-8")
            self._insert_fragment(file_id, commit_hash, FragmentType.text, content)

    def _insert_fragment(self, file_id: int, commit_hash: str, type: FragmentType, content: str) -> bool:
        cursor: sqlite3.Cursor = self._connection.cursor()

        # Create an embedding for the scope
        embedding: List[float]
        try:
            embedding = self._create_openai_embedding(content)
        except openai.APIError as e:
            logger.error(f"Error creating embedding: {e}")
            # TODO: if there are too many tokens, split the content into smaller parts and try again
            return False

        vector: bytes = serialize_float32(embedding)

        try:
            # Start a transaction
            cursor.execute("BEGIN;")
            # Insert the embedding and get the row_id
            cursor.execute(f"INSERT INTO {EMBEDDINGS_TABLE} (vector) VALUES (?)", (vector,))
            embedding_row_id = cursor.lastrowid  # Get the row_id of the inserted embedding

            # Use the row_id to insert into the documents table
            cursor.execute(
                f"""
                INSERT INTO {FRAGMENTS_TABLE}
                (file_id, type, content, embedding_id)
                VALUES (?, ?, ?, ?)
                """,
                (file_id, type.value, content, embedding_row_id),
            )

            # Commit the transaction
            cursor.execute("COMMIT;")
            return True
        except Exception as e:
            logger.error(f"Error inserting fragment: {e}")
            # Rollback the transaction in case of error
            cursor.execute("ROLLBACK;")
            raise e

    def _create_openai_embedding(self, input) -> List[float]:
        """
        Private method to create an embedding for a given code scope using OpenAI's API.
        """
        # Create the embedding
        client = openai.Client()
        response = client.embeddings.create(input=input, model=EMBEDDING_MODEL)
        return response.data[0].embedding

    def find_fragments(self, query: str, top_k: int, type: FragmentType | None) -> List[IndexSearchResult]:
        embedding: List[float] = self._create_openai_embedding(query)
        # Serialize the query vector
        query_vector: bytes = serialize_float32(embedding)

        cursor: sqlite3.Cursor = self._connection.cursor()
        # Execute the search query
        # LIMIT = top_k does not work on SQLite <3.41
        # https://github.com/asg017/sqlite-vec/issues/41#issuecomment-2241487294
        cursor.execute(
            f"""
            SELECT
                {FILES_TABLE}.path,
                {FILES_TABLE}.commit_hash,
                {FRAGMENTS_TABLE}.content
            FROM {FRAGMENTS_TABLE}
            JOIN {EMBEDDINGS_TABLE} ON {FRAGMENTS_TABLE}.embedding_id = {EMBEDDINGS_TABLE}.rowid
            JOIN {FILES_TABLE} ON {FRAGMENTS_TABLE}.file_id = {FILES_TABLE}.id
            WHERE
                {EMBEDDINGS_TABLE}.vector MATCH ?
                AND k = ?
            ORDER BY distance
            """,
            (query_vector, top_k),
        )

        # Fetch and return the results
        rows = cursor.fetchall()
        return [IndexSearchResult(file_name=row[0], commit_hash=row[1], content=row[2]) for row in rows]
