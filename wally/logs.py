import logging
import os
import sys

import coloredlogs
from pythonjsonlogger.json import JsonFormatter


def mask_sensitive_info(namespace, sensitive_keys):
    masked_dict = {}
    for k, v in vars(namespace).items():
        if k in sensitive_keys:
            masked_dict[k] = "*****"
        else:
            masked_dict[k] = v
    return masked_dict


def configure_logger(log_settings_str: str, format: str = "text") -> None:
    """
    Configures the logging system based on the specified log settings string.

    The log settings string should be a comma-separated list of module names and/or
    log levels. If a module name is specified, the log level applies to that module
    and its submodules. If no module name is specified, the log level applies to the
    root logger.

    For example, the log settings string "requests=WARNING,urllib3=DEBUG" will set
    the log level for the 'requests' module to WARNING and the log level for the
    'urllib3' module and its submodules to DEBUG.

    :param log_settings_str: A comma-separated list of module names and/or log levels.
    :type log_settings_str: str
    """

    # The default level is WARNING.
    root_level = logging.WARNING
    # But there is some specific logging settings, then all the logs are muted by default.
    if log_settings_str:
        root_level = level = sys.maxsize

    if "COLOREDLOGS_LEVEL_STYLES" not in os.environ:
        coloredlogs.DEFAULT_LEVEL_STYLES = {
            "debug": {"color": "white", "bold": False, "faint": True},
            "info": {"color": "green", "bold": False},
            "warning": {"color": "yellow", "bold": False, "faint": False},
            "error": {"color": "red", "bold": False, "faint": False},
            "critical": {"color": "red", "bold": True, "faint": False},
        }
        coloredlogs.DEFAULT_FIELD_STYLES = {
            "asctime": {"color": "white"},
            "hostname": {"color": "magenta"},
            "levelname": {},
            "name": {"color": "blue"},
            "funcName": {"color": "blue", "faint": True},
            "programname": {"color": "cyan"},
            "username": {"color": "yellow"},
        }
    coloredlogs.install(
        level=logging.NOTSET,
        fmt=os.environ.get(
            "COLOREDLOGS_LOG_FORMAT",
            "%(asctime)s %(levelname)-8s %(name)s %(funcName)s %(message)s",
        ),
        level_styles=coloredlogs.DEFAULT_LEVEL_STYLES,
        field_styles=coloredlogs.DEFAULT_FIELD_STYLES,
        # Force colored output when running in GitLab CI.
        isatty=True if "GITLAB_CI" in os.environ else None,
    )
    logging.root.setLevel(root_level)

    logging.getLogger("openai").setLevel(logging.WARNING)
    logging.getLogger("markdown_it").setLevel(logging.WARNING)

    # autogen_logger = logging.getLogger(EVENT_LOGGER_NAME)
    # autogen_logger.addHandler(logging.StreamHandler())
    # autogen_logger.setLevel(logging.INFO)  # noqa: F821

    log_settings = log_settings_str.split(",")
    # root_level = logging.CRITICAL

    for setting in log_settings:
        if "=" in setting:
            # Split the setting into the module name and log level
            module, level_str = setting.split("=")
        else:
            module, level_str = [None, setting]

        # Convert the log level string to the corresponding log level constant
        level = getattr(logging, level_str.upper(), None)
        # root_level = min(root_level, level)

        if level:
            if module is not None:
                # If the module is a package, apply the log level to all its submodules
                for name in logging.root.manager.loggerDict:
                    if name == module or name.startswith(f"{module}."):
                        logger = logging.getLogger(name)
                        logger.setLevel(level)
                else:
                    # Set the log level for the specific module
                    logger = logging.getLogger(module)
                    logger.setLevel(level)
            else:
                logging.root.setLevel(level)

    if format == "json":
        formatter = JsonFormatter("{levelname}{asctime}{module}{funcName}{message}", style="{")
        root_logger = logging.getLogger()
        for handler in root_logger.handlers:
            handler.setFormatter(formatter)

        for _logger_name, logger in logging.root.manager.loggerDict.items():
            if isinstance(logger, logging.Logger):
                for handler in logger.handlers:
                    handler.setFormatter(formatter)
