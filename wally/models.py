from enum import Enum
from gitlab.v4.objects import List
from pydantic import BaseModel


class User(BaseModel):
    id: int
    username: str


class Note(BaseModel):
    id: int
    body: str
    author: User
    created_at: str
    updated_at: str
    project_id: int
    type: str


class Issue(BaseModel):
    iid: int
    title: str
    description: str
    state: str
    created_at: str
    updated_at: str
    closed_at: str | None
    closed_by: User | None
    author: User
    assignee: User | None
    assignees: List[User]
    project_id: int


class MergeRequest(BaseModel):
    iid: int
    title: str
    description: str
    state: str
    created_at: str
    updated_at: str
    closed_at: str | None
    closed_by: User | None
    author: User
    assignee: User | None
    assignees: List[User]
    project_id: int
    has_conflicts: bool
    source_branch: str
    target_branch: str


class Commit(BaseModel):
    id: str
    title: str
    author_name: str
    author_email: str
    # In GitLab’s commit API response, both committed_date and authored_date generally appear for
    # typical Git commits and are almost always present. The main reason GitLab labels them as “optional”
    # in some parts of the documentation is that Git itself allows commits with missing or malformed
    # metadata, or other unusual edge cases.
    authored_date: str | None = None
    committed_date: str | None = None
    committer_name: str
    committer_email: str
    created_at: str
    message: str


class Diff(BaseModel):
    diff: str
    new_path: str
    old_path: str
    a_mode: str | None = None
    b_mode: str
    new_file: bool
    renamed_file: bool
    deleted_file: bool


class IndexSearchResult(BaseModel):
    file_name: str
    commit_hash: str
    content: str


class FileStatus(Enum):
    pending = "pending"
    processing = "processing"
    done = "done"


class FragmentType(Enum):
    function = "function"
    text = "text"
