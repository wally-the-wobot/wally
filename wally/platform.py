from datetime import datetime
import logging
from typing import Any, Dict, List, Optional, Tuple, Union, cast
import gitlab
from pygments.lexers import get_lexer_for_filename
from gitlab.v4.objects import (
    CurrentUser,
    ProjectCommit,
    ProjectCommitDiscussion,
    ProjectCommitDiscussionNote,
    ProjectIssue,
    ProjectIssueDiscussion,
    ProjectIssueNote,
    ProjectMergeRequest,
    ProjectMergeRequestDiscussion,
    ProjectMergeRequestNote,
    Todo,
)
import unidiff

logger = logging.getLogger(__name__)


Note = ProjectMergeRequestNote | ProjectIssueNote | ProjectCommitDiscussionNote
DiscussionNote = Any
NoteTarget = ProjectMergeRequest | ProjectIssue | ProjectCommit
Discussion = ProjectMergeRequestDiscussion | ProjectIssueDiscussion | ProjectCommitDiscussion
Message = Dict[str, str]

RETRYABLE_GITLAB_HTTP_CODES = [
    429,  # Too Many Requests
    502,  # Bad Gateway
    503,  # Service Unavailable
    504,  # Gateway Timeout
]


def handle_gitlab_error(error: gitlab.GitlabHttpError) -> None:
    """
    Handle a GitlabHttpError.

    Args:
        error (GitlabHttpError): The error to handle.
    """
    if error.response_code in RETRYABLE_GITLAB_HTTP_CODES:
        logger.warning(f"A retryable GitLab HTTP error occurred: {str(error)}")
        raise RuntimeError(f"A retryable GitLab HTTP error occurred. Try again. {str(error)}")
    else:
        logger.error("A fatal GitLab HTTP error occurred: {str(error)}")
        logger.critical("Impossible to create reply note: manual cleanup might be required.")
        raise error


def parse_gitlab_date(input: str) -> datetime:
    return datetime.fromisoformat(input.replace("Z", "+00:00"))


def todo_must_be_discarded(todo: Todo, user: CurrentUser) -> bool:
    """
    Discard a todo if its action_name is not "directly_addressed" or "mentioned" or if the author is the bot itself.

    Args:
        todo (Todo): The todo to check.
        user (CurrentUser): The bot GitLab user.

    Returns:
        bool: True if the todo should be discarded, False otherwise.
    """
    supported_actions = ["directly_addressed", "mentioned"]
    if todo.author["id"] == user.id:
        logger.warning(f"[Todo {todo.id}] created by the bot itself.")
        return True

    if todo.action_name not in supported_actions:
        logger.warning(
            f"[Todo {todo.id}] action_name is not supported: "
            f"expected one of {supported_actions}, got {todo.action_name}"
        )
        return True

    return False


def mark_as_done(todo, dry_run):
    if dry_run:
        logger.warning(f"[Todo {todo.id}] Dry run: skip marking as done.")
        return True
    else:
        logger.debug(f"[Todo {todo.id}] Marking as done...")
        try:
            todo.mark_as_done()
            logger.info(f"[Todo {todo.id}] Successfully marked as done.")
            return True
        except gitlab.GitlabHttpError as e:
            handle_gitlab_error(e)

    return False


def get_language_name_from_filename(filename: str) -> Optional[str]:
    """
    Get the programming language name from a given filename using Pygments.

    Args:
        filename (str): The filename or file path to determine the programming language.

    Returns:
        Optional[str]: The name of the programming language in lowercase, or None if
                       the language cannot be determined or if the lexer's name is "text only".
    """
    try:
        lexer = get_lexer_for_filename(filename)
        language_name = lexer.name.lower()  # type: ignore
        return None if language_name == "text only" else language_name
    except Exception:
        return None


def extract_diff_lines(
    start_line: Optional[int],
    end_line: Optional[int],
    diff: str,
) -> List[str]:
    """Extracts the diff lines between the specified start and end lines.

    This function extracts the lines of the given diff that lie between the
    specified start_line and end_line (inclusive). The extracted lines include
    added, removed, and unchanged lines within the specified range.

    Args:
        start_line (Optional[int]): The starting line number (inclusive) of the range to extract.
        end_line (Optional[int]): The ending line number (inclusive) of the range to extract.
        diff (str): The diff string containing the changes between two file versions.

    Returns:
        List[str]: The extracted lines of the diff between the specified start_line and
            end_line, including added, removed, and unchanged lines.
    """

    # Parse the diff string
    parsed_diff = unidiff.PatchSet(diff)

    extracted_lines: List[str] = []

    # Iterate through each file in the PatchSet
    for file in parsed_diff:
        # Iterate through each hunk in the file
        for hunk in file:
            for line in hunk:
                old_line_num, new_line_num = line.source_line_no, line.target_line_no

                # Check if the line is within the specified ranges for old and new versions
                within_old_range = (
                    old_line_num is not None
                    and end_line is not None
                    and (start_line or end_line) <= old_line_num <= end_line
                )
                within_new_range = (
                    new_line_num is not None
                    and start_line is not None
                    and start_line <= new_line_num <= (end_line or start_line)
                )

                if within_old_range or within_new_range:
                    extracted_lines.append(str(line))

    return extracted_lines


def get_diff(
    merge_request_or_commit: Union[ProjectMergeRequest, ProjectCommit],
    path_old: str,
    path_new: str,
) -> Optional[str]:
    """
    Retrieves the diff of a specific file between two versions represented by a commit or a merge request.

    This function takes a ProjectCommit or a ProjectMergeRequest object, along with the old and new file paths.
    It finds the corresponding diff for the specified file paths and returns it as a string. If no matching
    diff is found, the function returns None.

    Args:
        merge_request_or_commit (Union[ProjectMergeRequest, ProjectCommit]): The commit or merge request to
            get the diff from.
        path_old (str): The old file path.
        path_new (str): The new file path.

    Returns:
        Optional[str]: The diff string for the specified file paths, or None if no matching diff is found.
    """
    diff = None

    if isinstance(merge_request_or_commit, ProjectCommit):
        commit = merge_request_or_commit
        for commit_diff in commit.diff():
            if commit_diff["old_path"] == path_old and commit_diff["new_path"] == path_new:
                diff = commit_diff["diff"]
                break
    else:
        merge_request = merge_request_or_commit
        changes: Any = merge_request.changes()
        for change in changes["changes"]:
            if change["old_path"] == path_old and change["new_path"] == path_new:
                diff = change["diff"]
                break

    if diff is not None:
        return f"--- a/{path_old}\n+++ b/{path_new}\n{diff}"

    return None


def get_note_start_and_end_lines(
    note: DiscussionNote,
) -> Tuple[Optional[int], Optional[int]]:
    """
    Extracts the start and end line numbers of a GitLab discussion note.

    This function takes a DiscussionNote object and determines the start and end line numbers
    of the note based on its position and line range attributes. It handles multiline notes,
    single line notes, and notes on added, removed, or unchanged lines.

    Args:
        note (DiscussionNote): The GitLab discussion note object.

    Returns:
        Tuple[Optional[int], Optional[int]]: A tuple containing the start and end line numbers of the note
            (inclusive). If the note is on an unchanged line or a mix of added and removed lines, the start
            and end lines will be set accordingly. If the note doesn't have line number information, the
            corresponding tuple element will be None.
    """
    # Multiline note.
    if note["position"]["line_range"] is not None:
        start_line_new = note["position"]["line_range"]["start"]["new_line"]
        start_line_old = note["position"]["line_range"]["start"]["old_line"]
        end_line_new = note["position"]["line_range"]["end"]["new_line"]
        end_line_old = note["position"]["line_range"]["end"]["old_line"]

        # A thread on one or more unchanged lines.
        if start_line_old is not None and start_line_new is not None:
            return (start_line_new, end_line_new)
        # A thread on a mix of added and removed lines.
        if start_line_old is not None and end_line_new is not None:
            return (start_line_old, end_line_new)
        # A thread on an added line.
        elif start_line_new is not None:
            return (start_line_new, end_line_new)
        # A thread on a removed line.
        elif start_line_old is not None:
            return (start_line_old, end_line_old)

    # Single line note.
    return (note["position"]["new_line"], note["position"]["old_line"])


def diff_note_to_codeblock(
    merge_request_or_commit: Union[ProjectMergeRequest, ProjectCommit],
    note: DiscussionNote,
) -> Optional[str]:
    """
    Assuming the note is a "diff note", this function fetches the merge request's changes
    and returns the lines targeted by the diff note. The returned string is formatted with
    syntax highlighting based on the file extension if possible.

    Args:
        merge_request (ProjectMergeRequest): A GitLab ProjectMergeRequest object.
        note (Note): A GitLab Note object representing the diff note.

    Returns:
        Optional[str]: A formatted string containing the targeted lines in the diff note,
                       with syntax highlighting applied if possible, or None if the targeted
                       lines are not found.
    """
    path_old = note["position"]["old_path"]
    path_new = note["position"]["new_path"]
    diff = get_diff(merge_request_or_commit, path_old, path_new)

    if diff is None:
        return None

    start_line, end_line = get_note_start_and_end_lines(note)
    targeted_lines = extract_diff_lines(start_line, end_line, diff)

    if len(targeted_lines) > 0:
        language = get_language_name_from_filename(path_new) or ""
        formatted_note = "".join(targeted_lines)
        formatted_note = f"```{language}\n{formatted_note}\n```"
        return formatted_note
    else:
        return None


def is_a_text_diff_note(note: DiscussionNote) -> bool:
    return note["type"] == "DiffNote" and note["position"]["position_type"] == "text"


def get_todo_by_id(
    gitlab: gitlab.Gitlab,
    todo_id: str | int,
    project_id: str | int | None = None,
    state: str | None = None,
) -> Todo:
    todos = gitlab.todos.list(project_id=project_id, state=state)
    todos = [todo for todo in todos if todo.id == todo_id]
    if len(todos) == 0:
        raise ValueError(f"Todo with id {todo_id} not found")
    else:
        return cast(Todo, todos[0])
