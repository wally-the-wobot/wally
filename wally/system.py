import argparse
import asyncio
import logging
import os
import re
from textwrap import dedent
import time
import traceback
from typing import List, cast

from autogen_agentchat.base import Handoff
from autogen_agentchat.conditions import HandoffTermination
from autogen_agentchat.teams import Swarm
from autogen_ext.models.openai import OpenAIChatCompletionClient
from gitlab import Gitlab, GitlabAuthenticationError, GitlabError, GitlabHttpError
from gitlab.v4.objects import CurrentUser, Todo
from humanize import precisedelta
import openai

from wally.agents import (
    DiscussionSummaryAgent,
    IssueDiscussionAgent,
    IssueManagementAgent,
    MergeRequestDiscussionAgent,
    MergeRequestManagementAgent,
    MergeRequestSummaryAgent,
    RepositoryManagementAgent,
    TodoAgent,
)
from wally.platform import (
    RETRYABLE_GITLAB_HTTP_CODES,
    parse_gitlab_date,
    todo_must_be_discarded,
)
from wally.logs import configure_logger


logger = logging.getLogger(__name__)

TERMINATION_TEXT = "TERMINATE"


async def run(args: argparse.Namespace):
    configure_logger(args.log_level)
    logger.debug(args)

    # Set the OpenAI API key
    os.environ["OPENAI_API_KEY"] = args.openai_api_key

    try:
        re.compile(args.namespace_filter)
    except re.error as e:
        logger.critical(f"Invalid regex pattern for namespace filter `{args.namespace_filter}`: {str(e)}")
        exit(1)

    if args.dry_run:
        logger.warning("Operating in dry-run mode: modifications will not be executed.")

    try:
        # Connect to GitLab instance
        gitlab_client = Gitlab(args.url, private_token=args.token)
        gitlab_client.auth()
    except GitlabAuthenticationError as e:
        logger.critical(f"A GitLab authentication error occurred: {str(e)}")
        exit(2)

    return await main_loop(args, gitlab_client)


def filter_todos_by_namespace_filter(todos: List[Todo], namespace_filter: str) -> List[Todo]:
    """
    Filter the given todos by namespace filter.
    """
    res: List[Todo] = []
    for todo in todos:
        if re.match(namespace_filter, todo.project["path_with_namespace"]):
            res.append(todo)
        else:
            logger.warning(f"Skipping todo {todo.id} due to namespace filter")
    return res


def filter_discarded_todos(todos: List[Todo], user: CurrentUser) -> List[Todo]:
    """
    Filter the given todos by discarded todos.
    """
    res: List[Todo] = []
    for todo in todos:
        if todo_must_be_discarded(todo, user):
            logger.warning(f"Skipping discarded todo {todo.id}")
        else:
            res.append(todo)
    return res


async def main_loop(args: argparse.Namespace, gitlab_client: Gitlab):
    start_time = time.time()
    while time.time() - start_time < args.max_run_time:
        try:
            user: CurrentUser = cast(CurrentUser, gitlab_client.user)

            # Get the todos
            logger.debug("Fetching todos...")
            fetch_time = time.time()
            todos: List[Todo] = cast(List[Todo], gitlab_client.todos.list(state="pending"))
            todos = sorted(todos, key=lambda todo: parse_gitlab_date(todo.created_at))
            num_todos = len(todos)
            logger.info(f"Fetched {len(todos)} todo(s)")
            todos = filter_discarded_todos(todos, user)
            todos = filter_todos_by_namespace_filter(todos, args.namespace_filter)
            num_remaining = len(todos)
            num_filtered = num_todos - num_remaining
            logger.info(f"Filtered out {num_filtered}/{num_todos} todos, {num_remaining} todo(s) remaining.")

            num_success = 0
            num_maybe = 0
            delta_time = 0
            if num_remaining > 0:
                tasks = []
                for todo in todos:
                    task = asyncio.create_task(handle_todo(gitlab_client, todo, args))
                    task.todo = todo  # type: ignore
                    tasks.append(task)

                # Wait for tasks to complete or until the remaining time is reached
                remaining_time = args.max_run_time - (time.time() - start_time)
                # Gather or as_completed
                # Wait for tasks to complete up to args.max_run_time seconds
                done, pending = await asyncio.wait(
                    tasks,
                    timeout=remaining_time,
                    return_when=asyncio.ALL_COMPLETED,
                )

                # Collect the results of completed tasks
                for task in done:
                    todo = cast(Todo, getattr(task, "todo", None))
                    try:
                        # If the task raised an exception, .result() will re-raise it
                        result = task.result()
                        num_success += result
                    except Exception as e:
                        logger.exception(f"[Todo {todo.id if todo else '?'}] Task failed with exception: {e}")

                num_maybe = 0  # "maybe" could mean: we tried to cancel them, or they timed out

                # Cancel the remaining (pending) tasks (if any) and handle them
                for task in pending:
                    todo = cast(Todo, getattr(task, "todo", None))
                    was_canceled = task.cancel()
                    remaining_time = args.max_run_time - (time.time() - start_time)

                    if was_canceled:
                        logger.warning(f"[Todo {todo.id}] Out of runtime: task cancelled.")
                    else:
                        logger.warning(
                            f"[Todo {todo.id}] Out of runtime: task cancel requested, but task is still running. "
                            f"Waiting up to {remaining_time:.2f}s for it to complete."
                        )
                        try:
                            # Wait again (briefly) for it to finish
                            await asyncio.wait_for(task, remaining_time)
                            logger.info(f"[Todo {todo.id}] Task completed after waiting.")
                            num_success += task.result()  # collect result
                        except asyncio.TimeoutError:
                            logger.error(f"[Todo {todo.id}] Task did not complete within the remaining time.")
                            num_maybe += 1
                        except asyncio.CancelledError:
                            # The task actually responded to cancellation
                            logger.warning(f"[Todo {todo.id}] Task was cancelled while waiting.")
                            num_maybe += 1

                logger.debug(f"num_success={num_success}, num_maybe={num_maybe}")

            # Log the results
            delta_time = time.time() - fetch_time
            logger.info(
                f"Successfully processed {num_success}/{num_remaining} todo(s) in "
                f"{precisedelta(int(delta_time))}, {num_maybe} might still succeed."
            )

            sleep_time = args.sleep_time - delta_time
            logger.debug(f"sleep_time = {sleep_time}")
            remaining_time = args.max_run_time - (time.time() - start_time)
            logger.debug(f"remaining_time = {remaining_time}")
            if remaining_time < sleep_time:
                logger.info("No runtime left: exiting gracefully.")
                exit(0)
            else:
                logger.info(f"Available runtime: {precisedelta(remaining_time)}.")
                if sleep_time > 0:
                    logger.info(f"Sleeping {precisedelta(sleep_time)}...")
                    time.sleep(sleep_time)
        except GitlabHttpError as e:
            logger.warning(f"GitLab API error: {e}")
        except Exception:
            # At this point, no matter what error actually happened, the only way to
            # potentially recover is to retry by running the app again.
            logger.critical(traceback.format_exc())
            exit(1)


async def handle_todo(gitlab_client: Gitlab, todo: Todo, args: argparse.Namespace) -> bool:
    """
    Handles a single todo.
    """
    logger.info(f"[Todo {todo.id}] Handling todo...")

    try:
        logger.debug(f"[Todo {todo.id}] URL: {todo.target_url}")

        user: CurrentUser = cast(CurrentUser, gitlab_client.user)
        project = gitlab_client.projects.get(todo.project["id"])

        tools_model_client = OpenAIChatCompletionClient(
            model=args.openai_model,
            api_key=args.openai_api_key,
            tool_choice="required",  # type: ignore
            temperature=0.5,
        )
        model_client = OpenAIChatCompletionClient(
            model=args.openai_model,
            api_key=args.openai_api_key,
            temperature=0.5,
        )
        termination = HandoffTermination("root")

        repository_management_agent = RepositoryManagementAgent(
            name="repository_management_agent",
            model_client=tools_model_client,
            gitlab_client=gitlab_client,
            user=user,
            dry_run=args.dry_run,
            handoffs=[
                Handoff(
                    target="issue_discussion_agent",
                    description="Use this to handoff to the issue discussion agent when you are done.",
                    message="I have performed the necessary repository actions.",
                ),
                Handoff(
                    target="merge_request_discussion_agent",
                    description="Use this to handoff to the merge request discussion agent when you are done.",
                    message="I have performed the necessary repository actions.",
                ),
            ],
        )
        summary_agent = DiscussionSummaryAgent(
            name="summary_agent",
            model_client=model_client,
            handoffs=[
                Handoff(
                    target="issue_discussion_agent",
                    description="Use this to handoff to the issue discussion agent when you are done.",
                    message="Here is the summary you asked for.",
                ),
                Handoff(
                    target="merge_request_discussion_agent",
                    description="Use this to handoff to the merge request discussion agent when you are done.",
                    message="Here is the summary you asked for.",
                ),
            ],
        )
        mr_management_agent = MergeRequestManagementAgent(
            name="mr_management_agent",
            model_client=tools_model_client,
            gitlab_client=gitlab_client,
            user=user,
            dry_run=args.dry_run,
            handoffs=[
                Handoff(
                    target="issue_discussion_agent",
                    description="Use this to handoff to the issue discussion agent when you are done.",
                    message="I have performed the necessary merge request actions.",
                ),
                Handoff(
                    target="merge_request_discussion_agent",
                    description="Use this to handoff to the merge request discussion agent when you are done.",
                    message="I have performed the necessary merge request actions.",
                ),
            ],
        )
        mr_summary_agent = MergeRequestSummaryAgent(
            name="mr_summary_agent",
            model_client=model_client,
            gitlab_client=gitlab_client,
            handoffs=[
                Handoff(
                    target="merge_request_discussion_agent",
                    description="Use this to handoff to the merge request discussion agent when you are done.",
                    message="Here is the summary of the merge request you asked for.",
                ),
            ],
        )
        issue_management_agent = IssueManagementAgent(
            name="issue_management_agent",
            model_client=tools_model_client,
            gitlab_client=gitlab_client,
            user=user,
            dry_run=args.dry_run,
            handoffs=[
                Handoff(
                    target="issue_discussion_agent",
                    description="Use this handoff when you are done.",
                    message="I have performed the necessary actions.",
                )
            ],
        )
        issue_discussion_agent = IssueDiscussionAgent(
            name="issue_discussion_agent",
            model_client=model_client,
            gitlab_client=gitlab_client,
            user=user,
            dry_run=args.dry_run,
            handoffs=[
                Handoff(
                    target="todo_agent",
                    description="Use this handoff when you are done replying to the user.",
                    message="I have replied to the user.",
                ),
                Handoff(
                    target=issue_management_agent.name,
                    description="Use this handoff to perform issue management actions (ex: open or close an issue).",
                    message="Please perform the necessary issue management actions.",
                ),
                Handoff(
                    target=mr_management_agent.name,
                    description=(
                        "Use this handoff to perform merge request management actions "
                        "(ex: open or close a merge request)."
                    ),
                    message="Please perform the necessary merge request management actions.",
                ),
                Handoff(
                    target=repository_management_agent.name,
                    description="Use this handoff to perform Git repository management actions.",
                    message="Please perform the necessary Git repository management actions.",
                ),
                Handoff(
                    name="read_repository_files",
                    target=repository_management_agent.name,
                    description="Use this handoff to read files from a Git repository.",
                    message="Please read the file(s).",
                ),
                Handoff(
                    target=summary_agent.name,
                    description="Use this handoff to summarize the discussion.",
                    message="Please summarize the discussion.",
                ),
            ],
        )
        mr_discussion_agent = MergeRequestDiscussionAgent(
            name="merge_request_discussion_agent",
            model_client=model_client,
            gitlab_client=gitlab_client,
            user=user,
            dry_run=args.dry_run,
            handoffs=[
                Handoff(
                    target="todo_agent",
                    description="Use this handoff when you are done replying to the user.",
                    message="I have replied to the user.",
                ),
                Handoff(
                    target=issue_management_agent.name,
                    description="Use this handoff to perform issue management actions (ex: open or close an issue).",
                    message="Please perform the necessary issue management actions.",
                ),
                Handoff(
                    target=mr_management_agent.name,
                    description=(
                        "Use this handoff to perform merge request management actions "
                        "(ex: open or close a merge request)."
                    ),
                    message="Please perform the necessary merge request management actions.",
                ),
                Handoff(
                    target=repository_management_agent.name,
                    description="Use this handoff to perform Git repository management actions.",
                    message="Please perform the necessary Git repository management actions.",
                ),
                Handoff(
                    name="read_repository_files",
                    target=repository_management_agent.name,
                    description="Use this handoff to read files.",
                    message="Please read the file(s).",
                ),
                Handoff(
                    target=summary_agent.name,
                    description="Use this handoff to summarize the discussion.",
                    message="Please summarize the discussion.",
                ),
                Handoff(
                    target=mr_summary_agent.name,
                    description="Use this handoff to summarize the merge request.",
                    message="Please summarize the merge request.",
                ),
            ],
        )
        todo_agent = TodoAgent(
            name="todo_agent",
            model_client=tools_model_client,
            gitlab_client=gitlab_client,
            user=user,
            handoffs=[
                Handoff(
                    target=issue_discussion_agent.name,
                    description="Use this handoff to reply to the user in an issue.",
                    message="Please reply to the user.",
                ),
                Handoff(
                    target=mr_discussion_agent.name,
                    description="Use this handoff to reply to the user in a merge request.",
                    message="Please reply to the user.",
                ),
                Handoff(
                    target="root",
                    description="Use this handoff when you have dealt with the todo.",
                    message="Todo is done.",
                ),
            ],
            dry_run=args.dry_run,
        )

        team = Swarm(
            termination_condition=termination,
            participants=[
                todo_agent,
                issue_discussion_agent,
                mr_discussion_agent,
                issue_management_agent,
                mr_management_agent,
                repository_management_agent,
                summary_agent,
                mr_summary_agent,
            ],
        )

        result = await team.run(
            task=dedent(f"""
                Please handle the following todo:
                
                GitLab instance URL: {gitlab_client.api_url}
                
                Todo ID: {todo.id}
                Todo action: {todo.action_name}
                Todo state: {todo.state}
                Todo target ID: {todo.target["iid"]}
                Todo target type: {todo.target_type}
                Todo target URL: {todo.target_url}
                
                Project ID: {project.id}
                Project name: {project.name_with_namespace}
                Project path: {project.path_with_namespace}
                Project default branch: {project.default_branch}
                Project description: {project.description or "N/A"}
            """)
        )
        logger.debug(result)
    except GitlabHttpError as e:
        if e.response_code in RETRYABLE_GITLAB_HTTP_CODES:
            logger.warning(f"[Todo {todo.id}] A retryable GitLab HTTP error occurred: {str(e)}")
        else:
            logger.error(f"[Todo {todo.id}] A fatal GitLab HTTP error occurred: {str(e)}")
            logger.critical(
                e,
                "A fatal GitLab HTTP error occurred",
                todo.id,
                args.dry_run,
            )
    except GitlabError as e:
        logger.critical(e, "A fatal GitLab error occurred", todo.id, args.dry_run)
    except (
        openai.RateLimitError,
        openai.APIConnectionError,
        openai.APIError,
        openai.APITimeoutError,
        openai.APIStatusError,
    ) as e:
        logger.warning(f"[Todo {todo.id}] A retryable OpenAI error occurred: {str(e)}")
    except openai.OpenAIError as e:
        logger.critical(e, "A fatal OpenAI error occurred", todo.id, args.dry_run)
    except Exception:
        # All the other exceptions (most likely?) come from the code and will be
        # fixed by fixing the code itself. So we want the corresponding todo
        # to be retried. Therefore, we do not consider those as "fatal" errors.
        stack_trace = traceback.format_exc()
        logger.error(f"[Todo {todo.id}] An error occurred:\n{stack_trace}")

    return True
