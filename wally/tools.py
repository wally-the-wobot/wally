import inspect
import logging
import re
from textwrap import dedent
from typing import Any, Callable, Dict, List, Optional, cast
from autogen_core.tools import FunctionTool
from gitlab import Gitlab
from gitlab.exceptions import GitlabHttpError
from gitlab.v4.objects import ProjectIssue, ProjectMergeRequest

from wally.index import RepositoryIndex
from wally.models import Commit, Diff, FragmentType, IndexSearchResult, Issue, MergeRequest, Note
from wally.platform import (
    get_language_name_from_filename,
    get_todo_by_id,
    handle_gitlab_error,
    diff_note_to_codeblock,
    is_a_text_diff_note,
)

logger = logging.getLogger(__name__)


class GitlabTool(FunctionTool):
    def __init__(self, dry_run: bool, gitlab_client: Gitlab, func: Callable):
        global_params = {
            "gitlab_client": gitlab_client,
            "dry_run": dry_run,
        }

        sig = inspect.signature(func)

        def wrapper(**kwargs):
            logger.info(f"Calling GitLab tool `{func.__name__}` with arguments {kwargs}")
            # Filter kwargs to only include parameters that exist in the function signature
            valid_kwargs = {k: v for k, v in global_params.items() if k in sig.parameters}
            # You may still merge or manipulate these kwargs if needed
            all_kwargs = {**kwargs, **valid_kwargs}  # or {**valid_kwargs, **something_else}
            return func(**all_kwargs)

        sig = inspect.signature(func)
        wrapper.__name__ = func.__name__
        wrapper.__doc__ = dedent(func.__doc__ or "")
        wrapper.__annotations__ = func.__annotations__
        wrapper.__signature__ = inspect.Signature(
            parameters=[param for param in sig.parameters.values() if param.name not in global_params.keys()],
            return_annotation=sig.return_annotation,
        )

        super().__init__(func=wrapper, description=wrapper.__doc__, name=wrapper.__name__)


def get_note_type(note: Dict[str, Any]) -> str:
    if note.get("system", False) is True:
        return "SystemNote"
    if note.get("type", None) is None:
        return "DiscussionNote"
    return note.get("type", "DiscussionNote")


def list_merge_request_notes(
    gitlab_client: Gitlab,
    project_id: str | int,
    merge_request_iid: str | int,
    discussion_id: Optional[str | int] = None,
    note_type: Optional[str] = "DiscussionNote",
) -> List[Note]:
    """
    List all notes in a merge request. "notes" can be user-created comments
    or system-generated messages (e.g., assignee changes). If note_type is specified,
    only return that type; otherwise, return all.

    Possible note_type values:
    - "DiscussionNote": Normal comments (default)
    - "DiffNote": Notes on diffs
    - "ImageDiffNote": Diff notes for images
    - "SuggestionDiffNote" / "SuggestionNote": Inline suggestions
    - "SystemNote": System-generated messages (e.g., MR created or closed)

    If discussion_id is provided, only return notes from that discussion.
    Return notes for the entire merge request otherwise.

    Args:
        project_id (str | int): Project ID containing the MR
        merge_request_iid (str | int): The MR's internal ID
        discussion_id (Optional[str | int]): Discussion thread ID (optional)
        note_type (Optional[str]): Note type to list. Defaults to "DiscussionNote"

    Returns:
        List[Note]: A list of note objects

    Raises:
        GitlabHttpError: On HTTP-related errors from GitLab
    """
    try:
        project = gitlab_client.projects.get(project_id)
        merge_request = project.mergerequests.get(merge_request_iid)

        notes: List[Dict[str, Any]] = []
        if discussion_id is not None:
            discussion = merge_request.discussions.get(discussion_id)
            # GitLab's discussions API returns attributes["notes"] directly
            notes = discussion.attributes["notes"]
        else:
            # merge_request.notes is a list of note objects
            mr_notes = merge_request.notes.list(all=True)
            # Convert each note object to a dictionary
            notes = [note.asdict() for note in mr_notes]

        # Filter by note_type if specified
        if note_type is not None:
            notes = [note for note in notes if get_note_type(note) == note_type]

        notes = [{**note, "type": get_note_type(note)} for note in notes]
        # Sort the resulting notes by creation date
        notes = sorted(notes, key=lambda x: x["created_at"])

        # Optionally, handle the first note if it is a text diff note
        if len(notes) > 0 and is_a_text_diff_note(notes[0]):
            codeblock = diff_note_to_codeblock(merge_request, notes[0])
            if codeblock is None:
                logger.warning("Failed to parse diff note as codeblock")
            else:
                notes[0]["body"] = f"{codeblock}\n\n{notes[0]['body']}"

        result: List[Note] = [Note(**note) for note in notes]
        logger.debug(f"Merge request notes: {notes}")

        return result

    except GitlabHttpError as e:
        handle_gitlab_error(e)

    return []


def post_merge_request_comment(
    reply_text: str,
    project_id: str | int,
    merge_request_iid: str | int,
    discussion_id: Optional[str | int],
    gitlab_client: Gitlab,
    dry_run: bool,
) -> bool:
    """
    Adds a reply note to a GitLab merge request.
    If `discussion_id` is provided, the reply note will be added to that discussion.

    Args:
        reply_text (str): The text of the reply note.
        project_id (str | int): The ID of the project containing the merge request.
        merge_request_iid (str | int): The internal ID (aka "iid") of the merge request.
        discussion_id (Optional[str | int]): The ID of the discussion to add the reply to.

    Returns:
        bool: True if the reply note was successfully created, False otherwise.
    """

    logger.debug("Creating the merge request note...")
    try:
        project = gitlab_client.projects.get(project_id)
        merge_request = project.mergerequests.get(merge_request_iid)
        notes = merge_request.notes
        if discussion_id is not None:
            discussion = merge_request.discussions.get(discussion_id)
            notes = discussion.notes
        if dry_run:
            logger.warning("Dry run: skipping merge request note creation.")
        else:
            notes.create({"body": reply_text})
            logger.info("Merge request note successfully created.")
        return True
    except GitlabHttpError as e:
        handle_gitlab_error(e)

    return False


def create_issue(
    title: str,
    description: str,
    project_id: str | int,
    gitlab_client: Gitlab,
    dry_run: bool,
) -> Optional[str | int]:
    """
    Create a new issue in a GitLab project.

    Args:
        title (str): The title of the issue.
        description (str): The description of the issue.
        project_id (str | int): The ID of the project.

    Returns:
        Optional[str | int]: The internal ID of the created issue, or None in dry run mode.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        if dry_run:
            logger.warning("Dry run: skipping issue creation.")
            return None
        else:
            issue = project.issues.create({"title": title, "description": description})
            logger.debug(f"Created issue: {issue}")
        return issue.iid
    except GitlabHttpError as e:
        handle_gitlab_error(e)
        raise e


def get_todo_discussion_id(
    todo_id: str | int,
    project_id: str | int,
    target_url: str,
    target_type: str,
    target_id: str | int,
    gitlab_client: Gitlab,
) -> str | int | None:
    """
    Get the discussion ID for a todo.

    Args:
        todo_id (str | int): The ID of the todo.
        project_id (str | int): The ID of the project containing the todo.
        target_url (str): The URL of the target.
        target_type (str): The type of the target.
        target_id (str | int): The ID of the target.

    Returns:
        str | int | None: The discussion ID, or None if not found.
    """
    logger.debug(f"[Todo {todo_id}] Finding discussion ID...")

    target: ProjectIssue | ProjectMergeRequest | None = None
    if target_type == "Issue":
        project = gitlab_client.projects.get(project_id)
        target = project.issues.get(target_id)
    elif target_type == "MergeRequest":
        project = gitlab_client.projects.get(project_id)
        target = project.mergerequests.get(target_id)
    else:
        logger.error(f"[Todo {todo_id}] Unsupported target_type: {target_type}")
        raise ValueError(f"[Todo {todo_id}] Unsupported target_type: {target_type}")

    note_id = None
    match = re.search(r"note_(\d+)", target_url)
    if match:
        note_id = int(match.group(1))
    else:
        logger.error(f"[Todo {todo_id}] Note ID not found in target_url")
        raise ValueError(f"[Todo {todo_id}] Note ID not found in target_url")

    discussions = target.discussions.list(all=True)
    for discussion in discussions:
        for note in discussion.attributes["notes"]:
            if note["id"] == note_id:
                return discussion.id

    return None


def get_issue_by_id(
    project_id: str | int,
    issue_iid: str | int,
    gitlab_client: Gitlab,
) -> Issue | None:
    """
    Get a GitLab issue by its internal ID (aka "iid").

    Args:
        project_id (str | int): The ID of the project containing the issue.
        issue_iid (str | int): The internal ID of the issue.

    Returns:
        Issue: The GitLab issue object.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        issue = project.issues.get(issue_iid)
        return Issue(**issue.asdict())
    except GitlabHttpError as e:
        handle_gitlab_error(e)


def list_issues(
    gitlab_client: Gitlab,
    project_id: str | int,
    per_page: int = 100,
    page: int = 1,
) -> List[Issue] | None:
    """
    List all issues in a GitLab project.

    Args:
        project_id (str | int): The ID of the project.
        per_page (int): Number of items to retrieve per request
        page (int): ID of the page to return (starts with page 1)

    Returns:
        List[Issue]: A list of issue objects.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        issues = project.issues.list(per_page=per_page, page=page)
        issues = [issue.asdict() for issue in issues]
        issues = [Issue(**issue) for issue in issues]
        return issues
    except GitlabHttpError as e:
        handle_gitlab_error(e)


def list_issue_notes(
    gitlab_client: Gitlab,
    project_id: str | int,
    issue_iid: str | int,
    discussion_id: Optional[str | int] = None,
    note_type: Optional[str] = "DiscussionNote",
) -> List[Note] | None:
    """
    List all notes (comments) in a GitLab issue, including system notes.
    If note_type is specified, only return notes of that type; otherwise, return all.

    Possible note_type values:
    - "DiscussionNote": Normal comments (default)
    - "SystemNote": Automatically generated notes (e.g., assignment changes)

    If discussion_id is provided, only return notes from that discussion; otherwise,
    return notes from the entire issue.

    Args:
      project_id (str|int): Project ID containing the issue.
      issue_iid (str|int): Internal issue ID.
      discussion_id (Optional[str|int]): Discussion ID to filter by. Defaults to None.
      note_type (Optional[str]): Filter by note type. Defaults to "DiscussionNote".

    Returns:
      List[Note]: A list of note objects.

    Raises:
      GitlabHttpError: On HTTP errors from GitLab.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        issue = project.issues.get(issue_iid)

        if discussion_id is not None:
            # Fetch notes directly from the specified discussion
            discussion = issue.discussions.get(discussion_id)
            notes = discussion.attributes["notes"]
        else:
            # Fetch all notes for the issue
            all_notes = issue.notes.list(all=True)
            notes = [note.asdict() for note in all_notes]

        # If note_type is provided, filter by that type
        if note_type is not None:
            notes = [note for note in notes if get_note_type(note) == note_type]

        notes = [{**note, "type": get_note_type(note)} for note in notes]
        # Sort notes by creation date (ascending)
        notes = sorted(notes, key=lambda n: n["created_at"])

        notes = [Note(**note) for note in notes]
        logger.debug(f"Issue notes: {notes}")

        return notes
    except GitlabHttpError as e:
        handle_gitlab_error(e)


def post_issue_comment(
    reply_text: str,
    project_id: str | int,
    issue_iid: str | int,
    discussion_id: Optional[str | int],
    gitlab_client: Gitlab,
    dry_run: bool,
) -> bool:
    """
    Adds a comment to a GitLab issue.
    If `discussion_id` is provided, adds the comment to the discussion specified by this ID.

    Args:
        reply_text (str): The text of the reply note.
        project_id (str | int): The ID of the project containing the issue.
        issue_iid (str | int): The internal ID (aka "iid") of the issue.
        discussion_id (Optional[str | int]): The ID of the discussion to add the reply to.

    Returns:
        bool: True if the reply note was successfully created, False otherwise.
    """
    logger.debug("Creating the issue note...")
    try:
        project = gitlab_client.projects.get(project_id)
        issue = project.issues.get(issue_iid)
        notes = issue.notes
        if discussion_id is not None:
            discussion = issue.discussions.get(discussion_id)
            notes = discussion.notes
        if dry_run:
            logger.warning("Dry run: skipping issue note creation.")
        else:
            notes.create({"body": reply_text})
            logger.info("Issue note successfully created.")
        return True
    except GitlabHttpError as e:
        handle_gitlab_error(e)

    return False


def update_issue_state(
    project_id: str | int,
    issue_iid: str | int,
    state_event: str,
    gitlab_client: Gitlab,
    dry_run: bool,
) -> bool:
    """
    Updates the state of a GitLab issue.

    Args:
        project_id (str | int): The ID of the project containing the issue.
        issue_iid (str | int): The internal ID (aka "iid") of the issue.
        state_event (str): The new state of the issue ("close" or "reopen").
    """
    try:
        project = gitlab_client.projects.get(project_id)
        issue = project.issues.get(issue_iid)
        issue.state_event = state_event
        if dry_run:
            logger.warning("Dry run: skipping merge request creation.")
        else:
            issue.save()
        return True
    except GitlabHttpError as e:
        handle_gitlab_error(e)
    return False


def list_merge_requests(
    gitlab_client: Gitlab,
    project_id: str | int,
    state: str = "opened",
) -> List[MergeRequest] | None:
    """
    Lists all merge requests in a GitLab project.

    Args:
        project_id (str | int): The ID of the project to list merge requests for.
        state (str, optional): The state of the merge requests to list. Defaults to "opened".

    Returns:
        List[MergeRequest]: A list of MergeRequest objects.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        merge_requests = project.mergerequests.list(state=state)
        merge_requests = [MergeRequest(**mr.asdict()) for mr in merge_requests]
        return merge_requests
    except GitlabHttpError as e:
        handle_gitlab_error(e)


def create_merge_request(
    project_id: str | int,
    title: str,
    description: str,
    source_branch: str,
    gitlab_client: Gitlab,
    target_branch: str,
    dry_run: bool,
) -> Optional[str | int]:
    """
    Creates a new merge request in a GitLab project.

    Args:
        project_id (str | int): The ID of the project to create the merge request in.
        title (str): The title of the merge request.
        description (str): The description of the merge request.
        source_branch (str): The source branch of the merge request.
        target_branch (str): The target branch of the merge request.

    Returns:
        Optional[str | int]: The internal ID of the created merge request, or None in dry run mode.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        if dry_run:
            logger.warning("Dry run: skipping merge request creation.")
        else:
            mr = project.mergerequests.create(
                data={
                    "title": title,
                    "description": description,
                    "source_branch": source_branch,
                    "target_branch": target_branch,
                }
            )
            logger.debug(f"Created merge request: {mr}")
        return mr.iid
    except GitlabHttpError as e:
        handle_gitlab_error(e)
    return None


def update_merge_request(
    project_id: str | int,
    merge_request_iid: str | int,
    title: Optional[str],
    description: Optional[str],
    state_event: Optional[str],
    gitlab_client: Gitlab,
    dry_run: bool,
) -> bool:
    """
    Update a merge request state in a GitLab project.

    Args:
        project_id (str | int): The ID of the project containing the merge request.
        merge_request_iid (str | int): The ID of the merge request to close.
        title (str): The new title of the merge request.
        description (str): The new description of the merge request.
        state_event (str): The state event of the merge request ("close" or "reopen").

    Returns:
        bool: True if the merge request was successfully closed, False otherwise.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        merge_request = project.mergerequests.get(merge_request_iid)
        if dry_run:
            logger.warning("Dry run: skipping merge request update.")
        else:
            if state_event is not None:
                merge_request.state_event = state_event
            if title is not None:
                merge_request.title = title
            if description is not None:
                merge_request.description = description
            merge_request.save()
        return True
    except GitlabHttpError as e:
        handle_gitlab_error(e)
    return False


def get_project_id_by_path_with_namespace(
    gitlab_client: Gitlab,
    project_path_with_namespace: str,
) -> str | int | None:
    """
    Get a project's ID by its path (including the namespace).

    Args:
        gitlab_client (Gitlab): The GitLab client.
        project_path_with_namespace (str): The path with namespace of the project.
            Example: "lx-industries/wally-the-wobot/wally".
    Returns:
        str | int: The project ID.
    """
    try:
        project = gitlab_client.projects.get(project_path_with_namespace)
        return project.id
    except GitlabHttpError as e:
        handle_gitlab_error(e)


def mark_todo_as_done(
    project_id: str | int,
    todo_id: str | int,
    gitlab_client: Gitlab,
    dry_run: bool,
) -> bool:
    """
    Marks a todo as done in a GitLab project.

    Args:
        project_id (str | int): The ID of the project containing the todo.
        todo_id (str | int): The ID of the todo to mark as done.

    Returns:
        bool: True if the todo was successfully marked as done, False otherwise.
    """
    if dry_run:
        logger.warning(f"[Todo {todo_id}] Dry run: skip marking as done.")
        return True
    else:
        logger.debug(f"[Todo {todo_id}] Marking as done...")
        try:
            todo = get_todo_by_id(gitlab_client, project_id=project_id, todo_id=todo_id)
            if todo is None:
                logger.warning(f"[Todo {todo_id}] Todo not found: skipping.")
                return False
            if todo.attributes["state"] == "done":
                logger.debug(f"[Todo {todo_id}] Already marked as done: skipping.")
                return True
            todo.mark_as_done()
            logger.debug(f"[Todo {todo_id}] Successfully marked as done.")
            return True
        except GitlabHttpError as e:
            handle_gitlab_error(e)

    return False


def read_repository_file(
    project_id: str | int,
    file_path: str,
    ref: str | None,
    gitlab_client: Gitlab,
) -> str:
    """
    Reads a file from a GitLab repository.

    Args:
        project_id (str | int): The ID of the project containing the file.
        file_path (str): The path of the file to read.
        ref (Optional[str]): The name of the branch, tag or commit to read from.

    Returns:
        str: The content of the file.
    """
    return read_repository_file_lines(
        project_id=project_id,
        file_path=file_path,
        ref=ref,
        gitlab_client=gitlab_client,
    )


def read_repository_file_lines(
    gitlab_client: Gitlab,
    project_id: str | int,
    file_path: str,
    start_line: int | None = None,
    end_line: int | None = None,
    ref: str | None = None,
) -> str:
    """
    Reads a file from a GitLab repository.
    If start_line and end_line are provided, only the specified range is returned.
    If only start_line is provided, only the specified line is returned.
    If only end_line is provided, then start_line is set to 1.
    Line numbering uses 1-based indexing. Thus, start_line must be >= 1 if specified.

    Args:
        project_id (str | int): The ID of the project containing the file.
        file_path (str): The path of the file to read.
        start_line (Optional[int]): The start line of the range to read. Line numbering starts at 1.
        end_line (Optional[int]): The end line of the range to read.
        ref (Optional[str]): The name of the branch, tag or commit to read from.

    Returns:
        str: The content (of specified lines) of the file.

    Raises:
        ValueError: if start_line is specified but <= 0
    """
    if start_line is not None and start_line < 1:
        raise ValueError("Line numbering starts at 1.")
    if end_line is not None and start_line is None:
        start_line = 1
    if end_line is not None and start_line is not None and end_line < start_line:
        raise ValueError("end_line must be grater than start_line")

    try:
        project = gitlab_client.projects.get(project_id)
        # Guess the language name.
        language = get_language_name_from_filename(file_path)
        # Fetch the file content.
        file_content = cast(bytes, project.files.raw(file_path=file_path, ref=ref))
        # Split the content into lines.
        file_content = file_content.decode("utf-8")
        if start_line is not None:
            lines = file_content.splitlines()  # type: ignore
            # Get the start and end line numbers (subtract 1 for 0-based indexing).
            start_line = int(start_line) - 1
            end_line = int(end_line) - 1 if end_line else start_line
            # Extract the corresponding lines.
            extracted_lines = lines[start_line : end_line + 1]
            extracted_lines = "\n".join(extracted_lines)
        else:
            extracted_lines = file_content
        # Join the extracted lines with newline characters
        return f"```{language}\n{extracted_lines}\n```"
    except GitlabHttpError as e:
        handle_gitlab_error(e)
    return ""


def search_functions(
    gitlab_client: Gitlab,
    project_id: str | int,
    search_query: str,
    top_k: int = 16,
) -> List[IndexSearchResult]:
    """
    Perform a semantic search on a GitLab project to find functions matching a search query.

    The search is performed using vector distance match on the repository indexed functions.
    Thus, make sure your search query is comprehensive and long enough to find the desired function.

    Args:
        project_id (str | int): The ID of the project to search in.
        search_query (str): The search query to use.
        top_k (int): The number of results to return.

    Returns:
        List[IndexSearchResult]: A list of IndexSearchResult objects.
    """
    index = RepositoryIndex(gitlab_client=gitlab_client, project_id=project_id)
    return index.find_fragments(search_query, top_k, FragmentType.function)


def search_text_documents(
    gitlab_client: Gitlab,
    project_id: str | int,
    search_query: str,
    top_k: int = 16,
) -> List[IndexSearchResult]:
    """
    Perform a semantic search on a GitLab project to find text documents matching a search query.
    This function can be used to find documentation and other text documents via semantic search.

    The search is performed using vector distance match on the repository indexed text documents.
    Thus, make sure your search query is comprehensive and long enough to find the desired text document.

    Args:
        project_id (str | int): The ID of the project to search in.
        search_query (str): The search query to use.
        top_k (int): The number of results to return.

    Returns:
        List[IndexSearchResult]: A list of IndexSearchResult objects.
    """
    index = RepositoryIndex(gitlab_client=gitlab_client, project_id=project_id)
    return index.find_fragments(search_query, top_k, FragmentType.text)


def list_repository_files(
    gitlab_client: Gitlab,
    project_id: int,
    pattern: str | None,
) -> List[str] | None:
    """
    List the files in a GitLab repository.
    If a pattern is provided, only files matching the pattern are returned.
    Otherwise, all files are returned.

    Args:
        gitlab_client (Gitlab): The GitLab client instance.
        project_id (int): The ID of the project to search within.
        pattern (str | None): The regular expression pattern to match file paths against.

    Returns:
        List[str]: A list of file paths that match the pattern.

    Raises:
        re.error: If the pattern is not a valid regex.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        matching_files = []
        filter = re.compile(pattern).search if pattern else lambda x: True
        for file in project.repository_tree(recursive=True, all=True, iterator=True):
            if filter(file["path"]):
                matching_files.append(file["path"])
        return matching_files
    except GitlabHttpError as e:
        return handle_gitlab_error(e)


def list_project_languages(
    gitlab_client: Gitlab,
    project_id: str | int,
) -> Dict[str, float] | None:
    """
    List the programming languages (and their usage percentage) used in a GitLab project.

    Args:
        project_id (str | int): The ID of the project to list the programming languages of.

    Returns:
        Dict[str, float]: A dictionary mapping programming languages to their usage percentage.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        languages = project.languages()
        return cast(Dict[str, float], languages)
    except GitlabHttpError as e:
        return handle_gitlab_error(e)


def get_merge_request_by_id(
    project_id: str | int,
    merge_request_id: str | int,
    gitlab_client: Gitlab,
) -> MergeRequest | None:
    """
    Retrieve a merge request by its ID in a GitLab project.

    Args:
        project_id (str | int): The ID of the project containing the merge request.
        merge_request_id (str | int): The ID of the merge request to retrieve.

    Returns:
        ProjectMergeRequest | None: The merge request object if found, otherwise None.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        merge_request = project.mergerequests.get(merge_request_id)
        return MergeRequest(**merge_request.asdict())
    except GitlabHttpError as e:
        return handle_gitlab_error(e)


def list_merge_request_commits(
    gitlab_client: Gitlab,
    project_id: str | int,
    merge_request_iid: str | int,
) -> List[Commit] | None:
    """
    List all commits in a merge request.

    Args:
        gitlab_client (Gitlab): The GitLab client instance.
        project_id (str | int): The ID of the project containing the merge request.
        merge_request_iid (str | int): The internal ID of the merge request.

    Returns:
        List[Commit]: A list of commit objects.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        merge_request = project.mergerequests.get(merge_request_iid)
        commits = merge_request.commits(all=True)
        commits = [Commit(**commit.asdict()) for commit in commits]
        return commits
    except GitlabHttpError as e:
        return handle_gitlab_error(e)


def list_commit_diffs(
    gitlab_client: Gitlab,
    project_id: str | int,
    commit_sha: str,
) -> List[Diff] | None:
    """
    List all diffs in a commit.

    Args:
        project_id (str | int): The ID of the project containing the commit.
        commit_sha (str): The SHA of the commit.

    Returns:
        List[Diff]: A list of diff objects.
    """
    try:
        project = gitlab_client.projects.get(project_id)
        commit = project.commits.get(commit_sha)
        diffs = commit.diff()
        diffs = [Diff(**diff) for diff in diffs]
        return diffs
    except GitlabHttpError as e:
        return handle_gitlab_error(e)
